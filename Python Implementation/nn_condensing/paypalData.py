__author__ = 'Genia'
import nn
import json
import subprocess
import cPickle as pickle
from dtw import dtw
import gc
import os
import psutil
#import editdistanceDTW
import editdistance
import numpy as np
import random
from scipy import spatial
from numpy import array, zeros, argmin, inf
from tabulate import tabulate
from sklearn.preprocessing import normalize
from sklearn.metrics.pairwise import euclidean_distances
np.set_printoptions(threshold=np.inf)

def extract_windows_from_file_fixed_length(file_path,window_size,windows_amount,random_starting_point=False):
    file_data = open(file_path, 'r')
    line_string=file_data.readline()
    windows_list=[]
    window=[]
    counter=0
    while line_string!="":
        #print line_string
        if "recorder >" not in line_string:
            #line_string=line_string[line_string.index(" ")+1:]
            line_string=line_string.split(" ")[2]
            window.append(line_string)
            counter+=1
        if counter >= window_size:
            counter=0
            print window
            windows_list.append(window)
            if len(windows_list)>=windows_amount:
                break
            window=[]
        line_string=file_data.readline()
    print "Finished reading file"
    return windows_list


class Window(object):
    """ window with systel calls and a tag
    """
    def __init__(self):
        self.windows = []
        self.tag = ""
        self.fileName = ""
    def getTag(self):
        return self.tag
    def getLine(self):
        return self.windows
    def getFileName(self):
        return self.fileName

class Markov_matrix(object):
    """ window with systel calls and a tag
    """
    def __init__(self,markov_matrix):
        self.matrix = markov_matrix[1]
        self.tag = markov_matrix[0]
        self.fileName = ""
        self.index=None
    def getTag(self):
        return self.tag
    def getLine(self):
        return self.matrix
    def getFileName(self):
        return self.fileName
    def getIndex(self):
        return self.index




from itertools import (takewhile,repeat)
def rawincount(filename):
    f = open(filename, 'rb')
    bufgen = takewhile(lambda x: x, (f.read(1024*1024) for _ in repeat(None)))
    return sum( buf.count(b'\n') for buf in bufgen )

# def rawincount(f):
#     bufgen = takewhile(lambda x: x, (f.read(1024*1024) for _ in repeat(None)))
#     return sum( buf.count(b'\n') for buf in bufgen )
def testwccount():

    from os import walk,path
    for (dirpath, dirnames, filenames) in walk("C:\Users\Ilya\Desktop\sysdig_traces\sysdig_traces"):
        if dirpath.split("\\")[-1] != "PaxHeader":
            for file in filenames:
                print rawincount(path.join(dirpath, file))
            print "end loop"



def extract_windows_from_file_by_heartbeat_length(file_path,windows_amount,lines_limit=inf,fixed_length=None,random_start_point=False):
    #file_data = open(file_path, 'r')
    extract_windows_from_file_by_heartbeat_length.line_counter=1
    extract_windows_from_file_by_heartbeat_length.empty_window = False
    bufsize = 65536
    def process(line_string):
        #print line_string
        if ("recorder >" in line_string and fixed_length is not None): pass
        if ("recorder >" in line_string) or (fixed_length is not None and len(window.windows) == fixed_length):
            if len(window.windows) > 0:
                if tuple(window.windows) not in windows_set:
                    windows_list.append(window)
                    windows_set.add(tuple(window.windows))
            #window = Window()
            extract_windows_from_file_by_heartbeat_length.empty_window = True
            if len(windows_list) >= windows_amount:
                return False
            return True
        else:
            split_line = line_string.split(' ')
            sys_call=split_line[2]
            tag = line_string.split(' ')[0]
            if "_" in tag:
                split_tag=tag.split("_")
                if split_tag[0]=="async":
                    tag = "async"
                else:
                    if split_tag[1]=="scheduler":
                        tag = "scheduler"
            window.tag = tag
            if sys_call!='procinfo' and sys_call!='sched_yield':
                extract_windows_from_file_by_heartbeat_length.line_counter += 1
                window.windows.append(sys_call)
                if fixed_length is not None and len(window.windows) == fixed_length: #start a new window
                    if tuple(window.windows) not in windows_set:
                        windows_list.append(window)
                        windows_set.add(tuple(window.windows))
                    extract_windows_from_file_by_heartbeat_length.empty_window = True
                    if len(windows_list) >= windows_amount:
                        return False
                    return True
            return True
    windows_list=[]
    windows_set=set()
    window = Window()
    window.fileName=file_path
    skip_random = True
    if random_start_point and fixed_length is not None:
        skip_random = False
        import linecache
        file_length = rawincount(file_path)
        if fixed_length<4*windows_amount*fixed_length:
            skip_random=True
        else:
            minimal_buffer = 2*windows_amount*fixed_length
            random_starting_line_number = random.randint(0, file_length-minimal_buffer)
            check = True
            line_number = random_starting_line_number
            while line_number < fixed_length:
                line = linecache.getline(file_path, line_number)
                line_number+=1
                #print "trying to process",window
                res = process(line)
                #print "line counter",extract_windows_from_file_by_heartbeat_length.line_counter,lines_limit,res
                if (not res) or extract_windows_from_file_by_heartbeat_length.line_counter > lines_limit:
                    break
                if extract_windows_from_file_by_heartbeat_length.empty_window:
                    extract_windows_from_file_by_heartbeat_length.empty_window = False
                    window = Window()
                    window.fileName=file_path
    if skip_random:
        with open(file_path) as infile:
            check = True
            while check:
                lines = infile.readlines(bufsize)
                if not lines:
                    break
                for line in lines:
                    #print "trying to process",window
                    res = process(line)
                    #print "line counter",extract_windows_from_file_by_heartbeat_length.line_counter,lines_limit,res
                    if (not res) or extract_windows_from_file_by_heartbeat_length.line_counter > lines_limit:
                        check = False
                        break
                    if extract_windows_from_file_by_heartbeat_length.empty_window:
                        extract_windows_from_file_by_heartbeat_length.empty_window = False
                        window = Window()
                        window.fileName=file_path
    if len(windows_list) < windows_amount and len(window.windows)!=0 and tuple(window.windows) not in windows_set and (fixed_length is None or len(windows_list)==0):
        windows_list.append(window)
    print "Finished reading file"
    return windows_list


def analyze_distances(windows_list):
    distances = zeros((len(windows_list), len(windows_list)))
    sum = 0
    for i in range(len(windows_list)):
        #print "".join(windows_list[i])+" window # "+str(i)
        for j in range(len(windows_list)):
            dtw(windows_list[i], windows_list[j], dist=lambda x, y: 0 if (x == y) else 1)
            distances[i,j]=dtw(windows_list[i], windows_list[j], dist=lambda x, y: 0 if (x == y) else 1)[0]
            sum += distances[i,j]
    print sum
    print sum/(len(windows_list)*len(windows_list))
    print tabulate(distances, tablefmt="fancy_grid")



def measure_distance(window1,window2):
    return 0 if window1[0] == window2[0] else 1
    #return dtw(window1, window2, dist=lambda x, y: 0 if (x == y) else 1)[0]
    #return 1(window1,window2)
    #return editdistance.eval(window1,window2)
    #return editdistanceDTW.eval(window1,window2)
    #return edit_distance_python(window1,window2)
    #return discrete_measure(window1,window2)

def discrete_measure(window1,window2):
    if (len(window1)>len(window2)):
        window1,window2 = window2, window1
    discrete = map(lambda x,y:1 if x!=y else 0, window1,window2[0:len(window1)])
    return sum(discrete)+len(window2)-len(window1)

def find_distance(window, window_list):
    print len(window)
    min_distance = inf
    if len(window_list) == 0:
        min_distance = inf
    else:
        for element_window in window_list:
            print len(element_window)
            measure = measure_distance(element_window, window)
            print "distance between these windows is "+str(measure)
            #print "measured distance from window is = "+ str(measure)
            if measure < min_distance:
                min_distance = measure
                if min_distance==0:
                    break
    print "min dis of window from w.list is  "+str(min_distance)
    return min_distance

def edit_distance_python(str1, str2):
    size1 = len(str1)
    size2 = len(str2)
    stacks_rows=[]
    set_rows=[]
    for x in range(size1+1):
        stacks_rows.append([])
        set_rows.append(set())
    stacks_columns=[]
    set_columns=[]
    for x in range(size2+1):
        stacks_columns.append([])
        set_columns.append(set())

    d = zeros((size1+1, size2+1),dtype=np.int)
    p=np.chararray((size1+1, size2+1))
    d[0,0] = 0
    p[0,0]=""
    for i in range(1,size1+1):
        p[i,0]='u'
        if i>1 and str1[i-1] == str1[i-2]:
            d[i,0] = d[i-1,0]
        else:
            if str1[i-1] in set_columns[0]:
                while stacks_columns[0][-1]!=str1[i-1]:
                    set_columns[0].remove(stacks_columns[0].pop())
                d[i,0] = d[i-1,0]
            else:
                d[i,0] = d[i-1,0]+1
                stacks_columns[0].append(str1[i-1])
                set_columns[0].add(str1[i-1])

    for j in range(1,size2+1):
        p[0,j]="l"
        if j>1 and str2[j-1] == str2[j-2]:
            d[0,j] = d[0,j-1]
        else:
            if str2[j-1] in set_rows[0]:
                while stacks_rows[0][-1]!=str2[j-1]:
                    set_rows[0].remove(stacks_rows[0].pop())
                d[0,j] = d[0,j-1]
            else:
                d[0,j] = d[0,j-1]+1
                stacks_rows[0].append(str2[j-1])
                set_rows[0].add(str2[j-1])

    for i in range(1,size1+1):
        for j in range(1,size2+1):
            #print "status at point i,j ",i,j,set_rows[i],set_columns[j]
            up_addition=0
            left_addition = 0
            if not (i>1 and str1[i-1] == str1[i-2]):
                if str1[i-1] in set_columns[j]:
                    while stacks_columns[j][-1]!=str1[i-1]:
                        set_columns[j].remove(stacks_columns[j].pop())
                else:
                    up_addition=1
                    stacks_columns[j].append(str1[i-1])
                    set_columns[j].add(str1[i-1])
            up = d[i-1,j]+up_addition

            if not (j>1 and str2[j-1] == str2[j-2]):
                if str2[j-1] in set_rows[i]:
                    while stacks_rows[i][-1] != str2[j-1]:
                        set_rows[i].remove(stacks_rows[i].pop())
                else:
                    left_addition = 1
                    stacks_rows[i].append(str2[j-1])
                    set_rows[i].add(str2[j-1])
            left = d[i,j-1]+left_addition

            diag = d[i-1,j-1] + (0 if str1[i-1] == str2[j-1] else 1)
            d[i,j] = min(up,left,diag)
            p[i,j] = "u" if up==d[i,j] else "l" if left==d[i,j] else "d"
            if d[i,j]!=up:
                stacks_columns[j][:] = [] #empty the list
                stacks_columns[j].append(str1[i-1])
                set_columns[j].clear() #empty the set
                set_columns[j].add(str1[i-1])
            if d[i,j]!=left:
                stacks_rows[i][:] = [] #empty the list
                stacks_rows[i].append(str2[j-1])
                set_rows[i].clear() #empty the set
                set_rows[i].add(str2[j-1])
    # print d
    # print p
    # print set_columns
    # print stacks_columns
    # print set_rows
    # print stacks_rows

    return d[size1,size2]

    #print edit_distance_python(['s','s'],['s'])
    #print editdistance.eval('s','ssss' )

if __name__ == "__main__":
    # import sys
    # sys.exit(0)
    # print "ok"
    #windows_list_java = extract_windows_from_file("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\java\\27018",10,10)
    # print edit_distance_python("bananna","bbaannxaannnnaa")
    # import random, string
    # def get_random_string(length=12, allowed_chars='abcdefg' ):
    #     return ''.join(random.choice(allowed_chars) for i in range(length))
    # def randomword(length):
    #     return ''.join(random.choice(string.lowercase) for i in range(length))
    # def generate_random_word(min_length,max_length):
    #     return randomword(random.randint(min_length, max_length))
    # max_error=0
    # for x in range(1):
    #     if x % 10000 ==0: print "10k DONE ! ",str(x)
    #     word1=get_random_string(length=random.randint(50, 5000))
    #     word2=""
    #     word3=get_random_string(length=random.randint(5000, 5000))
    #     #print "testing "+word1,word2,word3+" max_error= "+str(max_error)
    #     d12=measure_distance(word1,word2)
    #     d21=measure_distance(word2,word1)
    #     d13=measure_distance(word1,word3)
    #     d32=measure_distance(word2,word3)
    #     print "distances are "+str(d12),str(d21),str(d13),str(d32)
    #     if d12!=d21 or d12>d13+d32:
    #         max_error = max(max_error,d12-(d13+d32))
    #         if (d12-(d13+d32)>1):
    #             print "Error with words "+word1,word2,word3+" error is " +str(d12-(d13+d32))+" max error = ",max_error
    def buildWindows():
        try:
            with open("C:\Python_output\\fileNames.pickle",'rb') as fileName:
                f = pickle.load(fileName)
                print "loaded filenames"
        except IOError:
            print("Oops!  no fileNames file")
            f = []
            from os import walk,path
            for (dirpath, dirnames, filenames) in walk("C:\Users\Ilya\Desktop\sysdig_traces\sysdig_traces"):
                if dirpath.split("\\")[-1] != "PaxHeader":
                    for file in filenames:
                        f.append(path.join(dirpath,file))
                    print "end loop"
                    with open("C:\Python_output\\fileNames.pickle",'wb') as fileName:
                        print "writing file names to disk"
                        pickle.dump(f,fileName)
        print "len f",len(f)
        windows=0
        total_windows_list=[]
        sample_windows_list=[]
        test_windows_list = []
        for filepath in f:
            window_list = extract_windows_from_file_by_heartbeat_length(filepath,100,lines_limit=1000000)
            print "window list length "+str(len(window_list))
            if len(window_list)>0:
                random_sample = random.sample(window_list, len(window_list)/3+1)
                sample_windows_list.extend(random_sample[:len(random_sample)/3+1])
                test_windows_list.extend(random_sample[len(random_sample)/3+1:])
                total_windows_list+=window_list
                windows += len(window_list)
        print "total windows amount (limited to 100 windows per file) = ", windows
        with open("C:\Python_output\windows_Sample.pickle",'wb') as f:
            print "writing windows object2 to disk"
            pickle.dump(sample_windows_list,f)
        with open("C:\Python_output\windows_Test.pickle",'wb') as f:
            print "writing windows object3 to disk"
            pickle.dump(test_windows_list,f)
        return sample_windows_list,test_windows_list



    def basic_experiment():
        try:
        #     with open("C:\Python_output\windows_Total.pickle",'rb') as f:
        #         total_windows_list = pickle.load(f)
            with open("C:\Python_output\windows_Sample.pickle",'rb') as f:
                print "opening sample"
                sample_windows_list = pickle.load(f)
            with open("C:\Python_output\windows_Test.pickle",'rb') as g:
                test_windows_list = pickle.load(g)
        except IOError:
            print("Oops!  That was no valid sample list.  Try again...")
            sample_windows_list,test_windows_list = buildWindows()
        print "sample windows size is  "+str(len(sample_windows_list))
        print "test windows size is  "+str(len(test_windows_list))
        #print "full system calls amount is ",sum(map(lambda x: len(x.getLine()), total_windows_list))
        print "sample system calls amount is ",sum(map(lambda x: len(x.getLine()), sample_windows_list))
        print "test system calls amount is ",sum(map(lambda x: len(x.getLine()), test_windows_list))
        #sample=nn.epsilon_net(sample_windows_list,measure_distance, time_function=True )["compressed_sample"]
        #print "e-net size",len(sample)
        #pruned=nn.consistent_pruning(sample,measure_distance)
        #print len(pruned)
        #windows_list_test = random.sample(sample_windows_list, 100)
        #print "test windows amount "+str(len(windows_list_test))
        print "result of running is "+str(nn.execute(sample_windows_list, test_windows_list, measure_distance))

    def markov_distance(mat1,mat2):
        return spatial.distance.minkowski(np.ravel(mat1), np.ravel(mat2), 1) #Lp distance


    def experiment2():
        markov_matrices = get_markov_matrices()
        print "got matrices"
        for mat_index,matrix in enumerate(markov_matrices):
            markov_matrices[mat_index] = Markov_matrix(matrix)
        print "read " + str(len(markov_matrices)) + " matrices"
        results=[]
        for i in range(10):
            np.random.shuffle(markov_matrices)
            train_data = markov_matrices[:int(len(markov_matrices)*0.50)]
            test_data = markov_matrices[int(len(markov_matrices)*0.50):]
            print "len",len(train_data),len(test_data)
            # sample = nn.epsilon_net(markov_matrices,markov_distance, time_function=True )["compressed_sample"]
            # print "sample size is ",len(sample)
            sample_hierarchy = nn.epsilon_net_hierarchy(train_data,markov_distance,time_function=True)
            print sample_hierarchy["work_time on net heirarchy"],"time hjierac"
            print "sample heirarchy is ",len(sample_hierarchy["compressed_sample"])
            pruned = nn.consistent_pruning(sample_hierarchy["compressed_sample"],markov_distance)
            print "pruned length ",len(pruned)
            result=nn.execute(pruned, test_data, markov_distance)
            results.append(result)
            print "result of running is "+str(result)
        print results
        print sum(results) / float(len(results))


    def experiment3(test_markov_matrices=None):
        # calculate the error of the sample  split by heartbeats when classified by full markov matrices
        markov_matrices = get_markov_matrices()
        print "got matrices"
        for mat_index,matrix in enumerate(markov_matrices):
            markov_matrices[mat_index] = Markov_matrix(matrix)
        print "read " + str(len(markov_matrices)) + " matrices"
        sample_hierarchy = nn.epsilon_net_hierarchy(markov_matrices,markov_distance,time_function=True)
        print sample_hierarchy["work_time on net heirarchy"],"time hjierac"
        print "sample heirarchy is ",len(sample_hierarchy["compressed_sample"])
        pruned = nn.consistent_pruning(sample_hierarchy["compressed_sample"],markov_distance)
        print "pruned length ",len(pruned)
        if test_markov_matrices is None:
            try:
                with open("C:\Python_output\windows_to_markov.pickle",'rb') as f:
                    print "opening sample"
                    test_markov_matrices = pickle.load(f)
                    print "test data in windows ",len(test_markov_matrices)
            except IOError:
                print("Oops!  That was no valid sample list.  Try again...")
                with open("C:\Python_output\windows_Sample.pickle",'rb') as g:
                    test_windows_list = pickle.load(g)
                    print "finished loading windows list",len(test_windows_list)
                    print "sample system calls amount is ",sum(map(lambda x: len(x.getLine()), test_windows_list))
                test_markov_matrices=[]
                sc_map = get_system_calls_map()
                for index, window in enumerate(test_windows_list):
                    if index%10==0: print "converted ",index," windows"
                    test_markov_matrices.append(calc_markov_matrix_from_window(window,sc_map))
                print "finished converting windows to markov matrices"
                with open("C:\Python_output\windows_to_markov.pickle",'wb') as f:
                    print "writing windows object4 to disk"
                    pickle.dump(test_markov_matrices,f)
        print "test size is "+str(len(test_markov_matrices))
        result=nn.execute(pruned, test_markov_matrices, markov_distance)
        print "result of running is "+str(result)

    def experiment4():
        # calculate the error of each file split by heartbeats when classified by full markov matrices
        markov_matrices = get_markov_matrices()
        print "got matrices"
        for mat_index,matrix in enumerate(markov_matrices):
            markov_matrices[mat_index] = Markov_matrix(matrix)
        print "read " + str(len(markov_matrices)) + " matrices"
        sample_hierarchy = nn.epsilon_net_hierarchy(markov_matrices,markov_distance,time_function=True)
        print sample_hierarchy["work_time on net heirarchy"],"time hjierac"
        print "sample heirarchy is ",len(sample_hierarchy["compressed_sample"])
        pruned = nn.consistent_pruning(sample_hierarchy["compressed_sample"],markov_distance)
        print "pruned length ",len(pruned)
        print("Oops!  That was no valid sample list.  Try again...")


        with open("C:\Python_output\\fileNames.pickle",'rb') as fileName:
            f = pickle.load(fileName)
        print "loaded filenames"
        print "len f",len(f)
        windows=0
        total=0
        sc_map = get_system_calls_map()
        for filepath in f:
            window_list = extract_windows_from_file_by_heartbeat_length(filepath,inf,lines_limit=16000000)
            print "window list length "+str(len(window_list))
            if len(window_list)>0:
                windows += len(window_list)
            test_markov_matrices=[]
            for index, window in enumerate(window_list):
                test_markov_matrices.append(calc_markov_matrix_from_window(window,sc_map))
            result=nn.execute(pruned, test_markov_matrices, markov_distance)
            print "result of this run is ",str(result)
            total += result*float(len(window_list))
            print "total average results so far ",total/windows
            process = psutil.Process(os.getpid())
            ram_usage = process.memory_info().rss
            print ram_usage
            gc.collect()
        print "finished reading files into windows"

        print "finished converting windows to markov matrices"
        print "result of running is "+str(total/windows)

    def calc_markov_matrix():
        from os import walk,path
        import random
        f = []
        for (dirpath, dirnames, filenames) in walk("C:\Users\Ilya\Desktop\sysdig_traces\sysdig_traces"):
            if dirpath.split("\\")[-1] != "PaxHeader":
                for file in filenames:
                    f.append(path.join(dirpath,file))
        sc_map = get_system_calls_map()
        markov_collection=[]
        for filepath in f:
            markov_matrix=calc_markov_matrix_from_filepath(filepath,sc_map,1000000)
            markov_collection = markov_collection + markov_matrix
        return markov_collection

    def calc_markov_matrix_from_filepath(filepath,sc_map=None,file_size_limit=inf):
        if sc_map is None:
            sc_map = get_system_calls_map()
        file_data = open(filepath, 'r')
        matrices_from_file=[]
        markov_matrix=["",np.zeros((89,89), dtype=np.float64)]
        previous_sc=""
        sys_call=""
        line_string=file_data.readline()
        data_counter=0
        while line_string!="":
            if data_counter >= file_size_limit:
                markov_matrix[1]=normalize(markov_matrix[1], axis=1, norm='l1')
                matrices_from_file.append(markov_matrix)
                data_counter = 0
                markov_matrix = [markov_matrix[0],np.zeros((89,89), dtype=np.float64)]
            if "recorder >" not in line_string:
                if sys_call!="":
                    previous_sc=sys_call
                split_line = line_string.split(' ')
                sys_call_test=split_line[2]
                if sys_call_test!='procinfo' and sys_call_test!='sched_yield' and sys_call_test!='clock_gettime' and sys_call_test!='switch':
                    sys_call=sys_call_test
                    if markov_matrix[0]=="":
                        tag = line_string.split(' ')[0]
                        if "_" in tag:
                            split_tag=tag.split("_")
                            if split_tag[0]=="async":
                                tag = "async"
                            else:
                                if split_tag[1]=="scheduler":
                                    tag = "scheduler"
                        markov_matrix[0]=tag
                    if previous_sc!="":
                        previous_sc_index = sc_map[previous_sc]
                        sys_call_index = sc_map[sys_call]
                        markov_matrix[1][previous_sc_index][sys_call_index] += 1
                        data_counter+=1
            line_string=file_data.readline()
        print "calc_markov_matrix_from_filepath  ",filepath
        if data_counter>0:
            markov_matrix[1]=normalize(markov_matrix[1], axis=1, norm='l1')
            matrices_from_file.append(markov_matrix)
        return matrices_from_file

    def calc_markov_matrix_from_window(window,sc_map=None):
        if sc_map is None:
            sc_map = get_system_calls_map()
        markov_matrix=["",np.zeros((89,89), dtype=np.float64)]
        markov_matrix[0]=window.getTag()
        previous_sc=""
        sc_list = window.getLine()
        for sc in sc_list:
            if previous_sc!="":
                previous_sc_index = sc_map[previous_sc]
                sys_call_index = sc_map[sc]
                markov_matrix[1][previous_sc_index][sys_call_index] += 1
            previous_sc=sc
        markov_matrix[1]=normalize(markov_matrix[1], axis=1, norm='l1')
        return_val = Markov_matrix(markov_matrix)
        return_val.fileName=window.fileName
        return return_val


    def test():
        from os import walk,path
        import random
        f = []
        for (dirpath, dirnames, filenames) in walk("C:\Users\Ilya\Desktop\sysdig_traces\sysdig_traces"):
            if dirpath.split("\\")[-1] != "PaxHeader":
                for file in filenames:
                    f.append(path.join(dirpath,file))
        for filepath in f:
            file_data = open(filepath, 'r')
            line_string=file_data.readline()
            while line_string!="":
                line_string=file_data.readline()
            print "Finished reading file"

    def calc_markov_matrix_numpy():
        from os import walk,path
        import random
        f = []
        sc_list=set()
        for (dirpath, dirnames, filenames) in walk("C:\Users\Ilya\Desktop\sysdig_traces\sysdig_traces"):
            if dirpath.split("\\")[-1] != "PaxHeader":
                for file in filenames:
                    f.append(path.join(dirpath,file))
        counter=0
        for filepath in f:
            file_data = open(filepath, 'r')
            line_string=file_data.readline()
            while line_string!="":
                if "recorder >" not in line_string:
                    sys_call= line_string.split(' ')[2]
                    sc_list.add(sys_call)
                line_string=file_data.readline()
            print "Finished reading file"
            counter+=1
        print sc_list
        print len(sc_list)

    def get_system_calls_map():
        sc_list = ['lseek', 'uname', 'epoll_create', 'getdents', 'mmap', 'arch_prctl', 'writev', 'fchmod', 'getpid', 'epoll_wait', 'fstat', 'procexit', 'read', 'getppid', 'setreuid', 'recvfrom', 'sendto',
             'clock_gettime', 'adjtimex', 'sendmmsg', 'connect', 'getsockname', 'close', 'shmget', 'rt_sigprocmask', 'setpgid', 'access', 'setsid', 'munmap', 'fcntl', 'lstat', 'keyctl', 'getcwd',
             'setregid', 'getpeername', 'sigreturn', 'shmat', 'setsockopt', 'msync', 'times', 'sendmsg', 'epoll_ctl', 'rt_sigaction', 'chmod', 'brk', 'shutdown', 'poll', 'open', 'select',
             'getsockopt', 'exit_group', 'nanosleep', 'shmdt', 'mkdir', 'faccessat', 'dup', 'sched_yield', 'shmctl', 'execve', 'socket', 'wait4', 'statfs', 'signaldeliver', 'getrusage',
             'timerfd_settime', 'setresuid', 'vfork', 'rename', 'procinfo', 'mprotect', 'accept', 'geteuid', 'switch', 'write', 'recvmsg', 'getrlimit', 'sendfile', 'stat', 'openat', 'clone',
             'ioctl', 'getegid', 'unlink', 'getuid', 'bind', 'alarm', 'futex', 'pipe', 'chown']
        return dict(zip(sc_list,range(len(sc_list))))




    def write_matrices_to_file(matrix_output,filepath):
        with open(filepath,'wb') as f:
            pickle.dump(matrix_output,f)


    def read_from_file_to_matrices(filepath):
        with open(filepath,'rb') as f:
            ret_obj = pickle.load(f)
        return ret_obj


     # matrix_output = calc_markov_matrix()
    #write_matrices_to_file(matrix_output,"C:\Python_output\markov_matrices.pickle")
    def get_markov_matrices():
        return read_from_file_to_matrices("C:\Python_output\markov_matrices.pickle")
    # [[tag,markov_mat],[tag,markov_mat],...]. ,markov_mat[i,j] = chance to go from sc_i to sc_j.

    class ComplexEncoder(json.JSONEncoder):
        def _byteify(data, ignore_dicts = False):
            #http://stackoverflow.com/questions/1207457/convert-a-unicode-string-to-a-string-in-python-containing-extra-symbols
            # if this is a unicode string, return its string representation
            if isinstance(data, unicode):
                return data.encode('utf-8')
            # if this is a list of values, return list of byteified values
            if isinstance(data, list):
                return [ ComplexEncoder._byteify(item, ignore_dicts=True) for item in data ]
            # if this is a dictionary, return dictionary of byteified keys and values
            # but only if we haven't already byteified it
            if isinstance(data, dict) and not ignore_dicts:
                return {
                    ComplexEncoder._byteify(key, ignore_dicts=True): ComplexEncoder._byteify(value, ignore_dicts=True)
                    for key, value in data.iteritems()
                }
            # if it's anything else, return it in its original form
            return data
        def default(self, obj):
            return obj.tag,obj.windows
        def decode(self,str):
            window = Window()
            js = json.loads(str)
            js = ComplexEncoder._byteify(js)
            window.tag = js[0]
            window.windows = js[1]
            return window

    #basic_experiment()
    #buildWindows()
    #experiment3()
    #testwccount()
    def countClasses():
        markov_matrices = get_markov_matrices()
        print "got matrices"
        aSet=set()
        for mat_index,matrix in enumerate(markov_matrices):
            markov_matrices[mat_index] = Markov_matrix(matrix)
            aSet.add(markov_matrices[mat_index].getTag())
        print aSet
        print len(aSet)

    def testCorrect():
        wind_list = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\python\\41231",inf,lines_limit=inf)
        print len(wind_list)
        win0,win1 = wind_list[0],wind_list[1]
        for window in wind_list:
            print type(window.getLine())," ",len(window.getLine())
        #print wind_list[0].getLine()[:100]
        #print wind_list[1].getLine()[:100]
        matrix0 = calc_markov_matrix_from_window(win0)
        matrix2 = calc_markov_matrix_from_window(win1)
        import hashlib
        hashed0 = hashlib.sha1(matrix0.getLine()).hexdigest()
        hashed2 = hashlib.sha1(matrix2.getLine()).hexdigest()
        # print len(win0.getLine())
        # print "@@@@@@@@@@@@@@@@@@"
        # print len(win2.getLine())
        # print "@@@@@@@@@@@@@@@@@@"
        # print matrix0.getLine()
        # print "@@@@@@@@@@@@@@@@@@"
        # print matrix2.getLine()
        # print "@@@@@@@@@@@@@@@@@@"
        print markov_distance(matrix0.getLine(),matrix2.getLine())
        print "@@@@@@@@@@@@@@@@@@"
        print hashed0
        print hashed2 #198ddef422384f56292ff8d49fa5d903a11c076f 268703ac601578c42e17af38854358bdf91c4f2c

    #write_matrices_to_file(calc_markov_matrix(),'C:\\Python_output\\markov_matrices.pickle')
    def extract_random_fixed_size_windows_from_data(windows_amount=10,window_length=30,random_start_point=False):
        with open("C:\Python_output\\fileNames.pickle",'rb') as fileName:
            f = pickle.load(fileName)
            print "loaded filenames"
        full_window_list=[]
        for filepath in f:
            window_list = extract_windows_from_file_by_heartbeat_length(filepath,windows_amount,fixed_length=window_length,random_start_point=random_start_point)
            print "window list length "+str(len(window_list))
            if len(window_list)>0:
                # print len(window_list[-1].getLine())
                print window_list[0].getFileName()
                full_window_list = full_window_list + window_list
                # print window_list[0].getLine()
        return full_window_list

    def calc_markov_matrices_from_data_fixed_size(window_length=30,windows_amount=10,random_start_point=False):
        window_list = extract_random_fixed_size_windows_from_data(windows_amount=windows_amount,window_length=window_length,random_start_point=random_start_point)
        markov_matrices_list = []
        print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        for window in window_list:
            markov_matrix = calc_markov_matrix_from_window(window)
            # if np.count_nonzero(markov_matrix)==0:
            #     print window.getLine()
            #     print window.getFileName()
            #     return
            markov_matrices_list.append(markov_matrix)
        with open("C:\Python_output\\markov_samples_length,"+str(window_length)+","+str(windows_amount)+".pickle",'wb') as fileName:
            print "writing file names to disk"
            pickle.dump(markov_matrices_list,fileName)
        return markov_matrices_list


    #calc_markov_matrices_from_data_fixed_size(window_length=1,windows_amount=10,random_start_point=True)
    # matrices_list = read_from_file_to_matrices("C:\Python_output\\markov_samples_length,60,3.pickle")
    # print len(matrices_list)
    # print "finished reading"
    # nn.find_min_dist(matrices_list,markov_distance)
    # sample_hierarchy = nn.epsilon_net_hierarchy(matrices_list,markov_distance,time_function=True)
    # print sample_hierarchy["work_time on net heirarchy"],"time hjierac"
    # print "sample heirarchy is ",len(sample_hierarchy["compressed_sample"])
    # pruned = nn.consistent_pruning(sample_hierarchy["compressed_sample"],markov_distance)
    # print "pruned length ",len(pruned)
    # result = nn.execute(pruned,matrices_list,markov_distance)
    # print "result of running is "+str(result)
    # with open("C:\Python_output\\markov_samples_length,60,3,pruned.pickle",'wb') as fileName:
    #     print "writing file names to disk"
    #     pickle.dump(pruned,fileName)

    #matrices_list = read_from_file_to_matrices("C:\Python_output\\markov_samples_length,60,3,pruned.pickle")
    def different_window_size_experiment():
        # full_markov = get_markov_matrices()
        # for mat_index,matrix in enumerate(full_markov):
        #     full_markov[mat_index] = Markov_matrix(matrix)
        # reduced_full = nn.epsilon_net_hierarchy(full_markov,markov_distance)
        # pruned_full = nn.consistent_pruning(reduced_full["compressed_sample"],markov_distance)
        # with open("C:\Python_output\\markov_samples_length,10,10.pickle",'rb') as f:
        #     print "opening sample markov_samples.pickle_length10"
        #     markov100000 = pickle.load(f)
        # print "10 length",len(markov100000)
        # markov100Kreduced = nn.epsilon_net_hierarchy(markov100000,markov_distance)
        # print "10 length",len(markov100Kreduced["compressed_sample"])
        # markov100Kpruned = nn.consistent_pruning(markov100Kreduced["compressed_sample"],markov_distance)
        # print "10 length",len(markov100Kpruned)
        # i=100000
        results = {}
        k=10
        data={}
        while k<=100000:
            with open("C:\Python_output\\markov_samples_length,"+str(k)+",10.pickle",'rb') as f:
                print "opening sample markov_samples.pickle_length,"+str(k)+",10"
                test_markov_matrices = pickle.load(f)
            with open("C:\Python_output\\markov_samples_length,"+str(k)+",10,pruned.pickle",'rb') as f:
                print "opening sample markov_samples.pickle_length,"+str(k)+",10"
                test_markov_matrices_pruned = pickle.load(f)
            # print "test data in windows ",len(test_markov_matrices)
            # reduced_test = nn.epsilon_net_hierarchy(test_markov_matrices,markov_distance)
            # print "reduces size ",len(reduced_test["compressed_sample"])
            # pruned_test = nn.consistent_pruning(reduced_test["compressed_sample"],markov_distance)
            # print "pruned ",len(pruned_test)
            # data[k]=[test_markov_matrices,reduced_test["compressed_sample"],pruned_test]
            # with open("C:\Python_output\\markov_samples_length,"+str(k)+",10,pruned.pickle",'wb') as fileName:
            #     print "writing file  to disk"
            #     pickle.dump(pruned_test,fileName)
            data[k]=[test_markov_matrices,test_markov_matrices_pruned]
            k*=10
        for key,data_set in data.iteritems():
            print "key =",key," length =",len(data_set[0]),len(data_set[1])
        i=10
        while i<=100000:
            j=10
            while j<=100000:
                results[(i,j)]=nn.execute(data[i][1],data[j][0],markov_distance)
                j*=10
            i*=10
        print results

        # results=[]
        # while i<10000001:
        #     try:
        #         with open("C:\Python_output\\markov_samples_length,"+str(i)+",10.pickle",'rb') as f:
        #             print "opening sample markov_samples.pickle_length,"+str(i)+",10"
        #             test_markov_matrices = pickle.load(f)
        #     except IOError:
        #         print("Oops!  That was no valid sample list.  Try again...")
        #         test_markov_matrices = calc_markov_matrices_from_data_fixed_size(window_length=i,windows_amount=1,random_start_point=False)
        #     print "test data in windows ",len(test_markov_matrices)
        #     reduced_test = nn.epsilon_net_hierarchy(test_markov_matrices,markov_distance)
        #     print "reduces size ",len(reduced_test["compressed_sample"])
        #     pruned_test = nn.consistent_pruning(reduced_test["compressed_sample"],markov_distance)
        #     print "pruned ",len(pruned_test)
        #     result = nn.execute(pruned_test,test_markov_matrices,markov_distance)
        #     print "result of classifying ~2000 windows of size "+str(i)+" is "+str(result)
        #     result = nn.execute(pruned_test,markov100000,markov_distance)
        #     print "result of classifying ~2000 windows of size "+str(i)+" is "+str(result)
        #     result = nn.execute(markov100Kpruned,test_markov_matrices,markov_distance)
        #     print "result of classifying ~2000 windows of size "+str(i)+" is "+str(result)
        #     results.append(result)
        #     break
        #     i=i*10
        # print results

    #different_window_size_experiment()
    def create_10():
        with open("C:\Python_output\\markov_samples_length,10,10.pickle",'rb') as f:
            print "opening sample markov_samples.pickle_length"
            test_markov_matrices = pickle.load(f)
        print "test data in windows ",len(test_markov_matrices)
        reduced_test = nn.epsilon_net(test_markov_matrices,markov_distance)
        print "reduces size ",len(reduced_test["compressed_sample"])
        pruned_test = nn.consistent_pruning(reduced_test["compressed_sample"],markov_distance,fast_radius=True)
        print "pruned ",len(pruned_test)
        with open("C:\Python_output\\markov_samples_length,10,10,pruned,greedy.pickle",'wb') as fileName:
            print "writing file  to disk"
            pickle.dump(pruned_test,fileName)
    #create_10()

    def test_10():

        with open("C:\Python_output\\markov_samples_length,10,10.pickle",'rb') as f:
            print "opening sample markov_samples.pickle_length"
            test_markov_matrices = pickle.load(f)
        with open("C:\Python_output\\markov_samples_length,100000,10.pickle",'rb') as f:
            print "opening sample markov_samples.pickle_length"
            markov10k = pickle.load(f)
        with open("C:\Python_output\\markov_samples_length,10,10,pruned,greedy.pickle",'rb') as f:
            print "opening sample markov_samples.pickle_length"
            markov_10_pruned = pickle.load(f)
        result = nn.execute(markov_10_pruned,test_markov_matrices,markov_distance)
        print "result of classifying ~2000 windows of size is "+str(result)
        result = nn.execute(markov_10_pruned,markov10k,markov_distance)
        print "result of classifying ~2000 windows of size 2is "+str(result)


    def test_SRM():
        with open("C:\Python_output\\windows_to_markov.pickle",'rb') as f:
            print "opening sample markov_samples.pickle_length"
            test_markov_matrices = pickle.load(f)
        for index,element in enumerate(test_markov_matrices):
            element.index = index
        print "length = ",len(test_markov_matrices)
        # split the data into train,test,validation
        gram = nn.get_gram_matrix(test_markov_matrices,"windows_to_markov",markov_distance)
        # with open("C:\Python_output\\"+str("100000,10")+"gram.pickle",'rb') as f:
        #     print "opening gram.pickle_length"
        #     gram = pickle.load(f)[1]
        res=[]
        for i in xrange(20):
            random.shuffle(test_markov_matrices)
            size = len(test_markov_matrices)
            train_data = test_markov_matrices[:int(size*0.6)]
            validation_data = test_markov_matrices[int(size*0.6):int(size*0.8)]
            test_data = test_markov_matrices[int(size*0.8):]
            print "train size =",len(train_data)
            print "valid size =",len(validation_data)
            print "test size =",len(test_data)
            compressed = nn.epsilon_net_hierarchy(train_data,markov_distance,gram_matrix=gram)
            print "compressed length = ",len(compressed["compressed_sample"])
            pruned = nn.consistent_pruning(compressed["compressed_sample"],markov_distance,gram_matrix=gram)
            print "pruned length = ",len(pruned)
            correct=1-nn.execute(pruned,test_data,markov_distance,gram_matrix=gram)
            print "execution result = ",correct
            res.append(correct)
        print res
        print np.mean(res)
        #nn.SRM(test_markov_matrices,markov_distance, data_description="100000,10")
        #print()


    def test_SVM():
        with open("C:\Python_output\\windows_to_markov.pickle",'rb') as f:
            print "opening sample markov_samples.pickle_length"
            test_markov_matrices = pickle.load(f)
        for index,element in enumerate(test_markov_matrices):
            element.index = index
        print "length = ",len(test_markov_matrices)
        # split the data into train,test,validation
        # with open("C:\Python_output\\"+str("100000,10")+"gram.pickle",'rb') as f:
        #     print "opening gram.pickle_length"
        #     gram = pickle.load(f)[1]
        from sklearn import svm
        from scipy import spatial
        c= 1
        import math
        import itertools
        while c<=1:
            clf = svm.SVC(kernel='precomputed',C=c)
            results=[]
            for i in xrange(20):
                random.shuffle(test_markov_matrices)
                size = len(test_markov_matrices)
                train_data = test_markov_matrices[:int(size*0.6)]
                validation_data = test_markov_matrices[int(size*0.6):int(size*0.8)]
                test_data = test_markov_matrices[int(size*0.8):]
                print "train size =",len(train_data)
                print "valid size =",len(validation_data)
                print "test size =",len(test_data)
                def getLabel(markov_m):
                    return markov_m.getTag()
                def getFeatures(markov_m):
                    return np.ravel(markov_m.getLine())
                x_train = map(getFeatures,train_data)
                y_train = map(getLabel, train_data)
                x_test = map(getFeatures,test_data)
                y_test = map(getLabel, test_data)
                # print y_train[:15]
                gamma=1
                # kernel_train=np.zeros((len(x_train),len(x_train)), dtype=np.float64)
                # i=0
                # j=1
                # for pair in itertools.combinations(x_train,2):
                #     #print pair
                #     kernel_train[i,j] = math.exp(-gamma*spatial.distance.euclidean(pair[0],pair[1]))
                #     j+=1
                #     if j==len(x_train):
                #         i+=1
                #         j=i+1
                kernel_train = np.dot(x_train, np.transpose(x_train))
                #print kernel_train
                clf.fit(kernel_train, y_train,)
                kernel_test = np.dot(x_test, np.transpose(x_train))
                y_pred = clf.predict(kernel_test)
                def countError(y1,y2):
                    if y1==y2: return 0
                    else: return 1
                res = map(countError,y_pred,y_test)
                #print y_test
                #print y_pred
                re = sum(res)/float(len(res))
                print 'Error : ', sum(res)/float(len(res))
                results.append(re)
            print results
            print "average error for c= ",(c)," ",sum(results)/float(len(results))
            c+=0.5
    # n=10000
    # d=1000
    # delta=0.05
    # e=0.01
    # import math
    # #e = (len(net)-len(temp_net))/float(len(net))
    # e_tag= e*n/(n-d)
    # X = (d+2)*math.log(n,2)+math.log(1/delta,2)
    # Q = e_tag+2/(3*(n-d))*X+math.sqrt(X*9*e_tag*(1-e_tag)/(2*(n-d)))
    # print Q
    test_SVM()
    #test_SRM()
    #test_10()
    # full_markov = get_markov_matrices()
    # for mat_index,matrix in enumerate(full_markov):
    #     full_markov[mat_index] = Markov_matrix(matrix)
    # reduced_full = nn.epsilon_net_hierarchy(full_markov,markov_distance)
    # pruned_full = nn.consistent_pruning(reduced_full["compressed_sample"],markov_distance)
    # print len(pruned_full)
    # print len(full_markov)#,len(matrices_list)
    # # print nn.find_min_dist(matrices_list,markov_distance)
    # # print nn.find_min_dist(pruned_full,markov_distance)
    # # print nn.find_min_dist(pruned_full+matrices_list,markov_distance)
    # matrices_list2 = read_from_file_to_matrices("C:\Python_output\\markov_samples_length,1,10.pickle")
    # print "100000000x1 size is",len(matrices_list2)
    # result = nn.execute(pruned_full,matrices_list2,markov_distance)
    # print str(result)


    #experiment3(test_markov_matrices= matrices_list)
    #experiment3()
    #countClasses()
    #testCorrect()
    # p1=calc_markov_matrix_from_filepath("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\python\\20908")
    # print "pre ",p1
    # with open("C:\Python_output\\a-file.pickle",'wb') as f:
    #     pickle.dump(p1,f)
    # with open("C:\Python_output\\a-file.pickle",'rb') as f:
    #     p2=pickle.load(f)
    # print "post ",p2
    # #print p1
    # p2=calc_markov_matrix_from_filepath("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\python\\20896")
    # p3=calc_markov_matrix_from_filepath("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\python\\20910")
    # #print p2
    # print spatial.distance.euclidean(np.ravel(p1[1]), np.ravel(p2[1]))
    # print spatial.distance.euclidean(np.ravel(p1[1]), np.ravel(p3[1]))
    # print spatial.distance.euclidean(np.ravel(p3[1]), np.ravel(p2[1]))
    # #print np.ravel(p1[1])
    # print spatial.distance.squareform(spatial.distance.pdist([np.ravel(p1[1]),np.ravel(p2[1]),np.ravel(p3[1])],'euclidean'))

    #test()
    #calc_markov_matrix()
    #print measure_distance(["d","monkey"],["daaabbcbbad","monkey"])
    # windows_list_python = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\python\\41222",200)
    # windows_list_python2 = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\python\\20908",200)
    # windows_list_sshd = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\sshd\\1595",inf)
    # windows_list_sshd2 = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\sshd\\29909",inf)
    # windows_list_epmd = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\epmd\\1790",inf)
    # windows_list_async_38 = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\\async_38\\9703",inf)
    # windows_list_in_30_scheduler = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\\30_scheduler\\9703",inf)
    # windows_list = windows_list_python + windows_list_in_30_scheduler+windows_list_async_38+windows_list_epmd+windows_list_sshd+windows_list_sshd2+windows_list_python2
    # print len(windows_list)
    # print sum(map(lambda x: len(x.getLine()), windows_list))
    # #print measure_distance(windows_list[0].getLine(),windows_list[1].getLine())
    # #print nn.find_radius(windows_list, measure_distance)
    # #print nn.find_min_dist(windows_list, measure_distance)
    # # sample=nn.epsilon_net(windows_list,measure_distance, time_function=True )["compressed_sample"]
    # # print len(sample)
    # # sample2 = nn.epsilon_net_hierarchy(windows_list,measure_distance, time_function=True )["compressed_sample"]
    # # print "sample2 ",len(sample2)
    # # pruned=nn.consistent_pruning(windows_list,measure_distance)
    # # print len(pruned)
    # windows_list_test_python = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\python\\20908",inf)
    # print len(windows_list_test_python)
    # print nn.execute(windows_list, windows_list_test_python, measure_distance)
    # print nn.execute(pruned, windows_list_test_python, measure_distance)
    # sample_window = extract_windows_from_file_by_heartbeat_length("C:\\Users\Ilya\Desktop\sysdig_traces\sysdig_traces\slccyactiveas02b\java\\27018",2)
    # # print len(windows_list_python)
    # distances = zeros(len(windows_list_python))
    # # print len(sample_window)
    # #for window in sample_window:
    # find_distance(sample_window[1],windows_list_python)
    #windows_list_pruned=[]
    #epsilon=0
    # for window in windows_list_python:
    #     dist = find_distance(window,windows_list_pruned)
    #     if dist>epsilon:
    #         windows_list_pruned.append(window)
    #analyze_distances(windows_list_pruned)
    #print len(windows_list_pruned)
