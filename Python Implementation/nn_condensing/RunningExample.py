__author__ = 'Genia'
import nn
import csv
import uuid
import math
from tabulate import tabulate
import os
import numpy as np
import matplotlib.pyplot as plt


#example distance_measure_L2([0,0,0],[3,4,5]) -> 7.07~...
def distance_measure_L2(data_a, data_b):
    def map_f(a, b):
        return (a-b)**2
    distances = map(map_f, data_a, data_b)
    distances = sum(distances)
    return math.sqrt(distances)

#example distance_measure_L1([0,0,0],[3,4,5]) -> 12
def distance_measure_L1(data_a, data_b):
    def map_f(a, b):
        return abs(a-b)
    distances = map(map_f, data_a, data_b)
    distance = sum(distances)
    return distance

def skin_line_parser(line):
        line = line.rstrip()
        float_arr = line.split()
        if float_arr[-1] == "1":
            return 1, [float(float_arr[0]), float(float_arr[1]), float(float_arr[2])]
        else:
            return 2, [float(float_arr[0]), float(float_arr[1]), float(float_arr[2])]

def run_experiment(
        data_path='E:\\1-NN repo\Data\Skin_NonSkin.txt', starting_train_data_size=100, test_data_size=200, train_data_upper_limit=3000, metric="L2",
        train_data_size_inc_func="geometric", train_data_size_inc_arg=2, compress_greedy= True, compress_heirarchy=True, test_memory=True, data_pruning_greedy=True, data_pruning_hierarchy=True,
        test_hierarchy_compression=True, test_1_nn_full_data=True, test_1_nn_greedy=True, test_1_nn_hierarchy=True, test_1_nn_greedy_pruning=True, test_1_nn_hierarchy_pruning=True, generate_CSV=True,
        CSV_path='',debug_info=True, print_tables=True, repeat=1, return_average_pruning_size=False, plot_compression=False
):
    data_pruning_greedy = data_pruning_greedy and compress_greedy
    data_pruning_hierarchy = data_pruning_hierarchy and compress_heirarchy
    test_hierarchy_compression = compress_heirarchy and test_hierarchy_compression
    test_1_nn_greedy = test_1_nn_greedy and compress_greedy
    test_1_nn_hierarchy = test_1_nn_hierarchy and compress_heirarchy
    test_1_nn_greedy_pruning = test_1_nn_greedy_pruning and data_pruning_greedy
    test_1_nn_hierarchy_pruning = test_1_nn_hierarchy_pruning and data_pruning_hierarchy
    if metric.lower() == "l2":
        data_metric = distance_measure_L2
    else:
        if metric.lower() == "l1":
            data_metric = distance_measure_L1
        else:
            data_metric = distance_measure_L2
    org_repeat=repeat
    if return_average_pruning_size:
        pruning_greedy_size_sum=0
    if plot_compression: #Then I will only run the experiment once for a single plot of the data.
        org_repeat=1
        repeat=1
    while repeat >= 1:
        print "   Experiment #%s" %(org_repeat-repeat+1)
        repeat = repeat - 1
        i = starting_train_data_size
        size_unknown = test_data_size
        loops = []
        time_construction_greedy = []
        size_greedy = []
        time_construction_hierarchy = []
        size_hierarchy = []
        pruning_greedy_size = []
        hierarchy_compression_success = []
        pruning_hierarchy_size = []
        construction_time_hierarchy_pruning = []
        construction_time_greedy_pruning = []
        test_success_full_data = []
        test_success_compressed_greedy = []
        test_success_compressed_hierarchy = []
        test_success_pruning_hierarchy = []
        test_success_pruning_greedy = []
        greedy_compression_memory = []
        hierarchy_compression_memory = []
        if plot_compression: #Then I will only run the experiment once for a single plot of the data.
            i = train_data_upper_limit
        while i <= train_data_upper_limit:
            size_known = i
            if debug_info:
                print " training size_hierarchy is = %s" %i
            skinDataSet, tagged_Data = nn.read_data_set(data_path, skin_line_parser)

            if debug_info:
                print "Finished reading data"
            known, unknown = nn.fill_data(tagged_Data, size_known, size_unknown)
            if debug_info:
                print "Finished filling data"

            if compress_greedy:
                compressed_data_greedy = nn.epsilon_net(known, data_metric,time_function=True, debug_information=debug_info, ram_information=test_memory)  #this is the condensing part - take the known sample and make a smaller subset of it
                if debug_info:
                    print "Finished constructing greedy net"
                time_construction_greedy.append(compressed_data_greedy["work_time"])
                size_greedy.append(len(compressed_data_greedy["compressed_sample"]))
                if test_memory:
                    greedy_compression_memory.append(compressed_data_greedy["ram_consumption"])

            if compress_heirarchy:
                if debug_info:
                    print "Started constructing hierarchy net"
                compressed_data_hierarchy = nn.epsilon_net_hierarchy(known, data_metric,time_function=True, debug_information=debug_info, ram_information=test_memory) #[compressed net, net construction time]
                if debug_info:
                    print "Finished constructing net with hierarchy size_hierarchy= %s" %len(compressed_data_hierarchy["compressed_sample"])
                    print("time_construction_hierarchy elapsed working on hierarchy net = %f seconds" % compressed_data_hierarchy["work_time"])
                time_construction_hierarchy.append(compressed_data_hierarchy["work_time"])
                size_hierarchy.append(len(compressed_data_hierarchy["compressed_sample"]))
                if test_memory:
                    hierarchy_compression_memory.append(compressed_data_hierarchy["ram_consumption"])

            if data_pruning_greedy:
                if debug_info:
                    print "working on pruning"
                net_after_pruning, time_greedy_pruning = nn.consistent_pruning(compressed_data_greedy["compressed_sample"], data_metric,time_function=True)
                pruning_greedy_size.append(len(net_after_pruning))
                construction_time_greedy_pruning.append(time_greedy_pruning)

            if data_pruning_hierarchy:
                if debug_info:
                    print "working on pruning"
                net_after_pruning_hierarchy,time_hierarchy_pruning = nn.consistent_pruning(compressed_data_hierarchy["compressed_sample"], data_metric,time_function=True)
                pruning_hierarchy_size.append(len(net_after_pruning_hierarchy))
                construction_time_hierarchy_pruning.append(time_hierarchy_pruning)

            if test_hierarchy_compression:
                if debug_info:
                    print("testing hierarchy compression")
                test_hierarchy_compression_result = nn.execute(compressed_data_hierarchy["compressed_sample"], known, data_metric)
                hierarchy_compression_success.append(test_hierarchy_compression_result)

            if test_1_nn_full_data:
                if debug_info:
                    print("testing with full data")
                test_1_nn_full_data_result = nn.execute(known, unknown, data_metric)
                test_success_full_data.append(test_1_nn_full_data_result)

            if test_1_nn_greedy:
                if debug_info:
                    print("testing with greedy compression")
                test_1_nn_greedy_result = nn.execute(compressed_data_greedy["compressed_sample"], unknown, data_metric)
                test_success_compressed_greedy.append(test_1_nn_greedy_result)

            if test_1_nn_hierarchy:
                if debug_info:
                    print("testing with hierarchy compression")
                test_1_nn_hierarchy_result = nn.execute(compressed_data_hierarchy["compressed_sample"], unknown, data_metric)
                test_success_compressed_hierarchy.append(test_1_nn_hierarchy_result)

            if test_1_nn_greedy_pruning:
                if debug_info:
                    print("testing with greedy compression+pruning")
                test_1_nn_greedy_pruning_result = nn.execute(net_after_pruning, unknown, data_metric)
                test_success_pruning_greedy.append(test_1_nn_greedy_pruning_result)

            if test_1_nn_hierarchy_pruning:
                if debug_info:
                    print("testing with hierarchy compression+pruning")
                test_1_nn_hierarchy_pruning_result = nn.execute(net_after_pruning_hierarchy, unknown, data_metric)
                test_success_pruning_hierarchy.append(test_1_nn_hierarchy_pruning_result)

            loops.append(i)
            if train_data_size_inc_func.lower() == "geometric":
                i = int(i * train_data_size_inc_arg)
            else:
                i = int(i + train_data_size_inc_arg)
        if print_tables:
            table = []
            table.append(["training size_hierarchy"]+loops)
            if compress_greedy:
                table.append(["greedy size"]+size_greedy)
                table.append(["greedy construction time"]+time_construction_greedy)
                table.append(["greedy size ratio"]+[float(x) / y for x, y in zip(size_greedy,loops)])
            if compress_heirarchy:
                table.append(["hierarchy size"]+size_hierarchy)
                table.append(["hierarchy construction time"]+time_construction_hierarchy)
                table.append(["hierarchy size ratio"]+[float(x) / y for x, y in zip(size_hierarchy,loops)])
            if data_pruning_greedy:
                table.append(["greedy pruning size"]+pruning_greedy_size)
                table.append(["greedy pruning construction time"]+construction_time_greedy_pruning)
            if data_pruning_hierarchy:
                table.append(["hierarchy pruning size"]+pruning_hierarchy_size)
                table.append(["hierarchy pruning construction time"]+construction_time_hierarchy_pruning)
            if test_hierarchy_compression:
                table.append(["compression success rate"]+hierarchy_compression_success)
            if test_1_nn_full_data:
                table.append(["test on full data success rate"]+test_success_full_data)
            if test_1_nn_greedy:
                table.append(["test on greedy net data success rate"]+test_success_compressed_greedy)
            if test_1_nn_hierarchy:
                table.append(["test on fast net data success rate"]+test_success_compressed_hierarchy)
            if test_1_nn_greedy_pruning:
                table.append(["test on greedy pruning data success rate"]+test_success_pruning_greedy)
            if test_1_nn_hierarchy_pruning:
                table.append(["test on fast net pruning data success rate"]+test_success_pruning_hierarchy)
            if test_memory:
                table.append(["memory consumption greedy compression(MB)"]+[x/1000000 for x in greedy_compression_memory])
                table.append(["memory consumption hierarchy compression(MB)"]+[x/1000000 for x in hierarchy_compression_memory])
            print tabulate(table, tablefmt="fancy_grid")
            if generate_CSV:
                #unique_filename = str(uuid.uuid4())
                if CSV_path == '':
                    SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
                    PARENT_ROOT=os.path.abspath(os.path.join(SITE_ROOT, os.pardir))
                    filename=os.path.abspath(os.path.join(PARENT_ROOT, 'Output/CSV_All_Data.CSV'))
                else:
                    filename=CSV_path
                print 'filename=' + filename
                with open(filename,"a+") as csvfile:
                    spamwriter = csv.writer(csvfile, dialect='excel')
                    formatted_table = zip(*table)
                    if os.stat(filename).st_size != 0:  #not empty
                        del formatted_table[0] #without the titles
                    spamwriter.writerows(formatted_table)
        if return_average_pruning_size:
            pruning_greedy_size_sum=pruning_greedy_size_sum+len(net_after_pruning)
        if plot_compression:
            compressed_data = compressed_data_hierarchy["compressed_sample"] if compress_heirarchy else compressed_data_greedy["compressed_sample"]
            pruned_data = net_after_pruning_hierarchy if data_pruning_hierarchy else net_after_pruning
            plot_compression_from_data(known,compressed_data ,pruned_data)
    if return_average_pruning_size:
        return float(pruning_greedy_size_sum)/org_repeat


def plot_data(filename='/home/genia/1-nn-condensing/Data/CSV_All_Data.CSV'):
    data = np.genfromtxt(filename, delimiter=',', skip_header=1, names=['a', 'b', 'c', 'd', 'e', 'f'])
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_title("Data Compression Rate")
    ax1.set_xlabel('Original Data Size')
    ax1.set_ylabel('Compressed Data Size')
    x, y, z = zip(*sorted((xVal, np.mean([yVal for a, yVal in zip(data['a'], data['b']) if xVal==a]),np.mean([yVal for a, yVal in zip(data['a'], data['e']) if xVal==a])) for xVal in set(data['a'])))
    ax1.plot(x, y, 'o-', color='r', label='greedy' )
    ax1.plot(x, z, 'o-', color='b', label='hierarchy')
    plt.show()


def plot_compression_from_data(test_data,compressed_data,pruned_compressed_data):
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    data={}
    for point in test_data:
        line=point.getLine()
        data[str(line[0])+','+str(line[1])+','+str(line[2])]={str(point.getTag()),'uncompressed'}
    for point in compressed_data:
        line=point.getLine()
        data[str(line[0])+','+str(line[1])+','+str(line[2])].add('compressed')
    for point in pruned_compressed_data:
        line=point.getLine()
        data[str(line[0])+','+str(line[1])+','+str(line[2])].add('pruned')
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    xs=[]
    ys=[]
    zs=[]
    colors=[]
    sizes=[]
    for point,info in data.iteritems():
        location=point.split(',')
        xs.append(float(location[0]))
        ys.append(float(location[1]))
        zs.append(float(location[2]))
        colors.append('darkslateblue' if ('1' in info and 'pruned' in info) else 'lightskyblue' if ('1' in info and 'compressed' in info) else 'b' if ('1' in info)
                      else 'darkorange' if ('pruned' in info)  else 'magenta' if ('compressed' in info)  else 'r')
        sizes.append(180 if 'pruned' in info else 60 if 'compressed' in info else 20)
    scat=ax.scatter(xs, ys, zs,s=sizes, c=colors, marker='o')
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')
    plt.show()


if __name__ == "__main__":
    skinDataSet, tagged_Data = nn.read_data_set('E:\\1-NN repo\Data\Skin_NonSkin.txt', skin_line_parser)
    n = 1000
    known, unknown = nn.fill_data(tagged_Data, n, n)
    compressed_data_greedy = nn.epsilon_net(known, distance_measure_L2,time_function=False, debug_information=False, ram_information=False)
    net_after_pruning = nn.consistent_pruning(compressed_data_greedy["compressed_sample"], distance_measure_L2 ,time_function=False,debug_information=True)
    data={}
    plot_compression_from_data(known,compressed_data_greedy["compressed_sample"],net_after_pruning)

