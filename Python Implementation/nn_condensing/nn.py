__author__ = 'Genia'
import random
import hashlib
from random import randrange
import numpy as np
import math
import time
import gc
import os
import psutil
import pickle


class DataSet(object):
    """ Container for data objects. Requires a 'line_parser' that reads a line from the input and translates it into
    a standard data line.
    """
    def __init__(self, line_parser=lambda x: x):
        self.line_parser = line_parser
        self.data = []
        self.size = 0


    def create_data_item(self, raw_line):
        """ Constructor of Data objects """
        tag, clean_line = self.line_parser(raw_line)
        data_item = Data(tag, clean_line)
        self.data.append(data_item)
        self.size += 1
        return data_item

    def create_data_item(self, clean_line, tag):
        """ Constructor of Data objects """
        data_item = Data(tag, clean_line)
        self.data.append(data_item)
        self.size += 1
        return data_item

    def __len__(self):
        return self.size

class Data(object):
    """ A data object is made from a data line that was already parsed by the DataSet.line_parser, and from a tag """
    def __init__(self, tag, line):
        self.tag, self.line = tag, line

    def getTag(self):
        return self.tag

    def getLine(self):
        return self.line

    def __repr__(self):
        """ For printing """
        return str(self.line)+" "+str(self.tag)


def read_data_set(full_path, line_parser):
    """ Reads the file at the full_path and constructs a DataSet using the distance_measure and
    line_parser parameters. For each line in the data create a Data object in the DataSet """
    file_data = open(full_path, 'r')
    line_string=file_data.readline()
    data_set = DataSet(line_parser)
    while line_string!="":
        data_set.create_data_item(line_string)
        line_string = file_data.readline()
    file_data.close()
    return data_set, data_set.data


def fill_data(tagged_Data, size_known, size_unknown):
    """ Fill two lists with data items randomly from a tagged data set  """
    known = []
    unknown = []
    known.extend(random.sample(tagged_Data, size_known))
    unknown.extend(random.sample(tagged_Data, size_unknown))
    return known, unknown


def find_min_dist(data_sample, distance_measure,gram_matrix=None):
    """ Find the minimum distance between any two items tagged differently in the data_sample  """
    min_dist = float("inf")  # infinity
    for i in xrange(len(data_sample)):
        for j in xrange(i + 1, len(data_sample)):  # iterate through all indexes, but starting from the index `i+1`
            if data_sample[i].getTag() == data_sample[j].getTag():  # we only count differently labeled examples
                continue
            if gram_matrix is not None:
                distance = gram_matrix[data_sample[i].getIndex(),data_sample[j].getIndex()]
            else:
                distance = distance_measure(data_sample[i].getLine(), data_sample[j].getLine())
            d = distance
            if d < min_dist:
                if d==0:
                    # print "distance is 0 ",data_sample[i].getTag(),data_sample[i].getLine(),data_sample[j].getTag(),data_sample[j].getLine()
                    # print np.count_nonzero(data_sample[i].getLine())
                    return 0
                min_dist = d
    return min_dist


def find_radius(data_set, distance_measure, starting_element_index=-1,fast_radius=False,gram_matrix=None):
    """ Find the maximum distance between the starting_element and the data_set or between any two items if no starting_element is given """
    radius = 0
    if starting_element_index == -1:
        for i in xrange(len(data_set)):
            for j in xrange(i + 1, len(data_set)):  # iterate through all indexes, but starting from the index `i+1`
                if gram_matrix is not None:
                    distance = gram_matrix[data_set[i].getIndex(),data_set[j].getIndex()]
                else:
                    distance = distance_measure(data_set[i].getLine(), data_set[j].getLine())
                d = distance
                if d > radius:
                    radius = d
    else:
        base_element = data_set[starting_element_index]
        for i in xrange(len(data_set)):
                if gram_matrix is not None:
                    distance = gram_matrix[data_set[i][i].getIndex(),base_element.getIndex()]
                else:
                    distance = distance_measure(data_set[i].getLine(), base_element.getLine())
                d = distance
                if d > radius:
                    radius = d
    return radius


def strong_proximity(index,T,distance,data,distance_measure,gram_matrix=None):
        """
        :return: [True,close_element] if distance_measure(data[index],T)<distance with close_element from T
                and [false,***] otherwise.
        """
        for element in T:
            if gram_matrix is not None:
                distance_m = gram_matrix[data[index].getIndex(),data[element].getIndex()]
            else:
                distance_m = distance_measure(data[index].getLine(), data[element].getLine())
            if distance_m < distance:
                return [True, element]
        return [False, 0]


def epsilon_net(data_sample, distance_measure, time_function=False, debug_information=False, ram_information=False,epsilon=None,gram_matrix=None):
    """ Build an epsilon net from the data_sample- greedy implementation  """
    returned_data = {}
    if time_function:
        starting_time = time.time()
    if epsilon == None:
        epsilon = find_min_dist(data_sample, distance_measure)  # finding the minimum distance between + and -
    #print "epsilon is ",epsilon
    if epsilon == 0:
        reduced_sample = data_sample
    else:
        reduced_sample = [random.choice(data_sample)]  # initiate the reduced sample with some element
        for data_item_in_sample in data_sample:
            insert = True
            for data_item_in_net in reduced_sample:  # check if the current element belongs to the net or not
                if gram_matrix is not None:
                    distance = gram_matrix[data_item_in_net.getIndex(),data_item_in_sample.getIndex()]
                else:
                    distance = distance_measure(data_item_in_net.getLine(), data_item_in_sample.getLine())
                if distance < epsilon:
                    #print "not inserting with distance ", distance, " items of the tag ",data_item_in_sample.getTag(),data_item_in_net.getTag()
                    insert = False
                    break  # no need to keep checking if we found it doesn't belong
            if insert:
                reduced_sample.append(data_item_in_sample)
    process = psutil.Process(os.getpid())
    ram_usage = process.memory_info().rss
    if debug_information:
        print "RAM consumption = %s" % ram_usage
    if time_function:
        ending_time = time.time()
        returned_data.update({"work_time on greedy net": ending_time-starting_time})
        print "work time " ,ending_time-starting_time
    if ram_information:
        returned_data.update({"ram_consumption": ram_usage})
    returned_data.update({"compressed_sample": reduced_sample})
    return returned_data


def epsilon_net_hierarchy(data_sample, distance_measure, epsilon=0, radius=0, time_function=False, debug_information=False, ram_information=False,gram_matrix=None):
    """ Build an epsilon net from the data_sample  """
    # Table S[i]- epsilon net with epsilon=(radius/2^i)
    # Table N[i]- lists of S[i] elements and their neighbours in S[i].
    # Table P[i]- list of data elements and their 'parent' in S[i]. A parent is an element whose distance is under S[i]'s epsilon.
    # Table C[i]- list of S[i] elements that are parents of S[i-1] elements, under the P[i] table definition, and their children in S[i-1].
    returned_value={}
    def _build_next_s(i, test_success):
    #build_next_s constructs the next level of the hierarchy. Assuming here we already have P,S,C,N etc defined in the main function.
    #return value: true if we need to keep running it, and false otherwise
    #print "loop i= %f" % i
        #starting_time_inner = time.time()
        #print S[i]
        #test if we have finished
        if test_success:
            found_compression = True
            for element_index in xrange(0, len(data_sample)):
                element_parent = P["level"][i]["origin"][element_index]
                if data_sample[element_parent].getTag() != data_sample[element_index].getTag():
                    #ignore agnostic cases
                    if gram_matrix is not None:
                        distance = gram_matrix[data_sample[element_parent].getIndex(),data_sample[element_index].getIndex()]
                    else:
                        distance = distance_measure(data_sample[element_parent].getLine(),data_sample[element_index].getLine())
                    if distance!=0:
                        #print "net is not ready. An item is not tagged correctly by its parent : element %s and %s their distance is %s" %(data_sample[element_parent].getLine(),data_sample[element_index].getLine(),distance_measure(data_sample[element_parent].getLine(),data_sample[element_index].getLine()))
                        print "building net with radius %s" %(radius*math.pow(2,i-1))
                        found_compression = False
                        break
            if found_compression:
                #print "we were supposed to run until the end of i= %s , but we finished at the start of i= %s" %(ratio-1, i)
                print "finished running after i= %s " % i
                #ending_time_inner = time.time()
                #print 'iteration i= %s time is %s' %(i,ending_time_inner-starting_time_inner)
                return False
        if i+1 in S:#clean unnecessary elements
            del S[i+1]
        if i+1 in C:#clean unnecessary elements
                del C[i+1]
        if i+1 in P:#clean unnecessary elements
                del P[i+1]
        if i+1 in N:#clean unnecessary elements
                del N[i+1]
        S[i-1] = S[i].copy()
        C["level"][i] = {"origin" : {}}
        P["level"][i-1] = {"origin" : {}}
        N["level"][i-1] = {"origin" : {}}
        for net_element_index in S[i-1]:
            C["level"][i]["origin"][net_element_index] = {net_element_index} #item in S is always the parent of itself
            if (net_element_index not in N["level"][i-1]["origin"]):#What is the point of this line? It should be added anyway in the next loop.
                        N["level"][i-1]["origin"][net_element_index]={net_element_index}
            # x is always neighbor of x
            #for all r ? N(p, i)
            for neighbor in N["level"][i]["origin"][net_element_index]:
                if gram_matrix is not None:
                    distance = gram_matrix[data_sample[net_element_index].getIndex(),data_sample[neighbor].getIndex()]
                else:
                    distance = distance_measure(data_sample[net_element_index].getLine(),data_sample[neighbor].getLine())
                if distance<2.5*radius*math.pow(2,i-1):
                    # N(p, i ? 1) ? N(p, i ? 1) ? {r}
                    # N(r, i ? 1) ? N(r, i ? 1) ? {p}
                    if (neighbor not in N["level"][i-1]["origin"]):
                        N["level"][i-1]["origin"][neighbor]=set()
                    N["level"][i-1]["origin"][net_element_index].add(neighbor)
                    N["level"][i-1]["origin"][neighbor].add(net_element_index)
        #for all q ? S do
        for element_index in xrange(0,len(data_sample)):
            T=set()
            element_parent=P["level"][i]["origin"][element_index]
            for neighbor_element in N["level"][i]["origin"][element_parent]: #do i need to initiate it?
                T.update(C["level"][i]["origin"][neighbor_element])
            #if d(q, T ) < 2^i-1
            proximity_answer=strong_proximity(element_index,T,radius*math.pow(2,i-1),data_sample,distance_measure,gram_matrix=gram_matrix)
            if proximity_answer[0]:# meaning element_index is close to T
                P["level"][i-1]["origin"][element_index] = proximity_answer[1] #If we want a guarantee for consistency here is where we need to find nearest neighbor and check labels matching.
            else:
                S[i-1].add(element_index)
                P["level"][i-1]["origin"][element_index] = element_index
                C["level"][i]["origin"][element_parent].add(element_index)
                N["level"][i-1]["origin"][element_index] = {element_index}
                for T_element in T:
                    if gram_matrix is not None:
                        distance = gram_matrix[data_sample[T_element].getIndex(),data_sample[element_index].getIndex()]
                    else:
                        distance = distance_measure(data_sample[T_element].getLine(),data_sample[element_index].getLine())
                    if distance<2.5*radius*math.pow(2,i-1):
                        N["level"][i-1]["origin"][element_index].add(T_element)
                        N["level"][i-1]["origin"][T_element].add(element_index)
        print "S[%s] size is %s " %(i-1,len(S[i-1]))
        #ending_time_inner = time.time()
        #print 'iteration i= %s time is %s' %(i,ending_time_inner-starting_time_inner)
        return True


    # epsilon = find_min_dist(data_sample, distance_measure)  # finding the minimum distance between + and -. OPTIONAL
    # print "min distance in hierarchy is %f " % epsilon
    starting_element = randrange(0,len(data_sample))  # randomly chosen first element
    if radius == 0:
        radius = find_radius(data_sample, distance_measure, starting_element) #OPTIONAL
    if debug_information:
        print "max distance in hierarchy  is %f " % radius
    if time_function:
        starting_time = time.time()
    if epsilon != 0:
        ratio = int(math.floor(math.log(epsilon/radius,2)))
        if debug_information:
            print "ratio in hierarchy is %f" % ratio
    S = {0:{starting_element}} # start with some element. 0 is chosen here. S[net level]-net elements
    N = {"level":{0:{"origin":{starting_element:{starting_element}}}}} # The neighbors of point #0 at level 0 is itself.
    #  N['level'][level index]['origin'][origin index]-neighbors elements
    C = {"level":{}}
    #  C[level][level index][origin][origin index]-origin's children
    P = {"level":{0:{"origin":{starting_element:starting_element}}}} # The parents table.
    for i in xrange(0,len(data_sample)):
        P["level"][0]["origin"][i]=starting_element # everyone's parent is the starting element.
    #assuming: When entering the i-th loop, N[i] is finished,S[i] is finished,C[i+1] is finished,P[i] is finished.
    starting_time_inner = time.time()
    if epsilon == 0:
        i = 0
        while _build_next_s(i, True):
            if ram_information:
                if "ram_consumption" not in returned_value:
                    returned_value["ram_consumption"]=0
                process = psutil.Process(os.getpid())
                ram_consumption = process.memory_info().rss
                if returned_value["ram_consumption"] < ram_consumption:
                    returned_value["ram_consumption"] = ram_consumption
                if debug_information:
                    print "RAM consumption = %s" % process.memory_info().rss
            i = i-1
            #gc.collect()
    else:
        for i in range(0,ratio,-1):
            _build_next_s(i, False)
            gc.collect()
    if time_function:
        ending_time = time.time()
        returned_value["work_time on net heirarchy"] = ending_time-starting_time
        print "work time " ,ending_time-starting_time
    returned_value["compressed_sample"] = [data_sample[index] for index in S[min(S)]]
    return returned_value


def consistent_pruning(net, distance_measure, debug_information=False, time_function=False,fast_radius=False,gram_matrix=None):
    """ Reduce a give epsilon net even further """
    if time_function:
        starting_time = time.time()
    new_net = list(net)  # the net to return
    temp_net = list(net)  # a temporary copy of a net for the while loop
    print "Finding min dist"
    starting_time = time.time()
    epsilon = find_min_dist(net, distance_measure,gram_matrix=gram_matrix)
    print "Found min dist = ",epsilon," time= ",time.time()-starting_time
    starting_time = time.time()
    pruning_density = 2  # number bigger than 1. Default = 2.
    print "Finding radius"
    if fast_radius:
        starting_element = randrange(0,len(net))  # randomly chosen first element
        radius = find_radius(net, distance_measure, starting_element,gram_matrix=gram_matrix)
    else:
        radius = find_radius(net, distance_measure,fast_radius=fast_radius,gram_matrix=gram_matrix)
    print "Found radius = ",radius," time= ",time.time()-starting_time
    if debug_information:
        print "max distance for pruning is %f " % radius
    if epsilon==0:
        rounding =0.001
    else:
        rounding = 1#0.0001 #  below this difference we don't bother calculating
    starting_time = time.time()
    print "pruning..."
    while radius >= epsilon+rounding:
        for net_item in temp_net:
            if net_item not in new_net:
                break  # meaning that we already deleted this item, and there is no need to go over it again
            canRemove = True
            for net_item_inner in new_net:
                if net_item.getTag() != net_item_inner.getTag():
                    if gram_matrix is not None:
                        distance = gram_matrix[net_item.getIndex(),net_item_inner.getIndex()]
                    else:
                        distance = distance_measure(net_item.getLine(), net_item_inner.getLine())
                    if distance < 2*radius:
                        canRemove = False
                        break
            if canRemove:
                for temp_net_item in temp_net:  # this is the place where I need 2 copies of the net to iterate+delete safely (without indexing problems)
                    if net_item is not temp_net_item:
                        if net_item.getTag() != net_item_inner.getTag():
                            if gram_matrix is not None:
                                distance = gram_matrix[temp_net_item.getIndex(),net_item.getIndex()]
                            else:
                                distance = distance_measure(temp_net_item.getLine(), net_item.getLine())
                            if distance < (radius - epsilon):# don't remove the actual point, only its neighbours
                                new_net.remove(temp_net_item)
        radius = float(radius) / pruning_density
        temp_net = new_net
    if time_function:
        ending_time = time.time()
        return new_net, ending_time-starting_time
    print "pruning...finished, time= ",time.time()-starting_time
    return new_net


def recognize(element, known, distance_measure,debug_information=False,gram_matrix=None):
    """ recognize the element using the known data (find the closest neighbor and return it and their distance)
    note - need to deal with the rare case of empty known array """
    min_distance_found = [-1, element]
    for known_element in known:
        if gram_matrix is not None:
            distance = gram_matrix[known_element.getIndex(),element.getIndex()]
        else:
            distance = distance_measure(known_element.getLine(), element.getLine())
        if min_distance_found[0] == -1 or distance < min_distance_found[0]:
            min_distance_found[0] = distance
            min_distance_found[1] = known_element
    if debug_information:
        print "result is %f with tag %s" % (min_distance_found[0], min_distance_found[1].getTag())
    return min_distance_found


def execute(known, unknown, distance_measure, debug_information=False,gram_matrix=None):
    """ For every item in the unknown list, recognize it using the 1-NN algo' with the given sample - known.
     Count the error %. """
    starting_time = time.time()
    error = 0
    count = 0
    if gram_matrix is not None:
        for element in unknown:
            count += 1
            if count % 100 == 0:
                if debug_information:
                    print "execution step is %s" %count
            reco_result = recognize(element, known, distance_measure,gram_matrix=gram_matrix)
            nn_Label = reco_result[1].getTag()
            if debug_information:
                print "element in question = %s. element found= %s. distance= %f" %(element.getLine(), reco_result[1].getLine(), reco_result[0])
            if element.getTag() != nn_Label:
                error += 1
    else:
        for element in unknown:
            count += 1
            if count % 100 == 0:
                if debug_information:
                    print "execution step is %s" %count
            reco_result = recognize(element, known, distance_measure)
            nn_Label = reco_result[1].getTag()
            if debug_information:
                print "element in question = %s. element found= %s. distance= %f" %(element.getLine(), reco_result[1].getLine(), reco_result[0])
            if element.getTag() != nn_Label:
                error += 1
        if debug_information:
            print "error count= %s" % error
    error = float(error)
    error /= len(unknown)
    correct = 1 - error
    ending_time = time.time()
    #print "work time on executing" ,ending_time-starting_time
    return correct
def get_gram_matrix(net,data_description,distance_measure):
    try:
        with open("C:\Python_output\\"+str(data_description)+"gram.pickle",'rb') as f:
            print "opening gram.pickle_length"
            ret = pickle.load(f)
            if type(ret) is tuple:
                return ret[1]
            else:
                return ret
    except IOError:
        print "error loading"
        Gram = np.zeros((len(net),len(net)), dtype=np.float64)
        for i in xrange(len(net)):
            for j in xrange(i + 1, len(net)):  # iterate through all indexes, but starting from the index `i+1`
                d = distance_measure(net[i].getLine(), net[j].getLine())
                Gram[i,j] = d
                Gram[j,i] = d
        with open("C:\Python_output\\"+str(data_description)+"gram.pickle",'wb') as fileName:
            print "writing file  to disk"
            pickle.dump(Gram,fileName)
        return Gram
def SRM(net,distance_measure,delta=0.05,data_description=None):

    """
    :param net: the data set
    :param distance_measure: the metric for items in the data
    :return: A table that consists of : 1. all the edges of the net that have differently tagged vertices, sorted by their distance
                                        2. For each edge, an indicator if this edge to be removed or not in the VC process
                                        3. For each edge, the margin that we get if we chose to remove it and all its predecessors.
                                        4. For each edge, calculate  Q(d,e) by compressing the subsample S' into size d, and calculating e- the
                                        error of S' about S.
                                        The formula: delta = 0.05 .E=e*n/(n-d). X = (d+2)*log(n)-log(delta). Q(d,e)= E+2/(3*(n-d))*X+sqrt(X*9*E*(1-E)/(2*(n-d)))
    """
    def find_edges(data,gram_matrix):
        edges = [[(0,0),-1,False,0]] #If we remove no edges
        for i in xrange(len(data)):
            for j in xrange(i + 1, len(data)): # iterate through all indexes, but starting from the index `i+1`
                if data[i].getTag()!=data[j].getTag():
                    d = gram_matrix[data[i].getIndex(),data[j].getIndex()]
                    edges.append([(i,j),d,None,0]) #indices,distance, to-remove,margin
        return edges
    print "get gram"
    gram = get_gram_matrix(net,data_description,distance_measure) #this returns a list of all the edges with the distances.
    random.shuffle(net)
    size = len(net)
    train_data = net[:int(size*0.6)]
    validation_data = net[int(size*0.6):int(size*0.8)]
    test_data = net[int(size*0.8):]
    print "train size =",len(train_data)
    print "valid size =",len(validation_data)
    print "test size =",len(test_data)
    print "get edges"
    edges = find_edges(train_data,gram)
    print "sort edges"
    sorted_edges = sorted(edges, key=lambda edge: edge[1])
    print "sort edges done"
    for idx, val in enumerate(sorted_edges):
        if idx!=0:
            sorted_edges[idx-1][3]=val[1] #the margin that we achieve by removing previous edge
    sorted_edges[-1][3]=float("inf") #margin is infinite if we removed all edges
    removed_vertices = set()
    for index,edge in enumerate(sorted_edges):
        if (edge[0][0] not in removed_vertices) and (edge[0][1] not in removed_vertices) and index!=0 : #we need to remove edge if so
            edge[2] = True
            removed_vertices.add(edge[0][0])
            removed_vertices.add(edge[0][1])
        else:
            edge[2] = False
    def find_best_subsets_by_real_Q_and_by_validation(train_d,sorted_edges,validation_d):
        temp_net = list(train_d)
        count = 0
        n = float(len(train_d))
        minQ=float("inf")
        minValidation=float("inf")
        bestSubsetByQ=None
        bestSubsetByValidation=None
        for idx, val in enumerate(sorted_edges):
            # compress the net
            if idx==len(sorted_edges)-1: break #the last example we dont check
            if val[2]==True:
                temp_net.remove(train_d[val[0][0]])
                temp_net.remove(train_d[val[0][1]])
            if val[1]<sorted_edges[idx+1][1]:
                if count%10==0:
                    # print "compressing"
                    # print "net 8 and 0 are"
                    # print net[8]
                    # print net[0]
                    compresses_sample = epsilon_net(temp_net,distance_measure,epsilon=val[3],gram_matrix=gram)
                    def calc_Q(n,e,d):
                        if (n-d)==0:
                            return float("inf")
                        e_tag= e*n/(n-d)
                        X = (d+2)*math.log(n,2)+math.log(1/delta,2)
                        Q = e_tag+2/(3*(n-d))*X+math.sqrt(X*9*e_tag*(1-e_tag)/(2*(n-d)))
                        return Q
                    e = (len(train_d)-len(temp_net))/float(len(train_d))
                    d= float(len(compresses_sample["compressed_sample"]))
                    Q = calc_Q(n,e,d)
                    result = 1-execute(compresses_sample["compressed_sample"],train_d,distance_measure,gram_matrix=gram)
                    realQ=calc_Q(n,result,d)
                    result_validation = 1-execute(compresses_sample["compressed_sample"],validation_data,distance_measure,gram_matrix=gram)
                    print "margin= ",val[3],"compressed sample size= ",len(compresses_sample["compressed_sample"]),"error bound= ",e,"Q = ",Q,"real error on "+str(n)+" points= ",result," real Q= ",\
                        realQ," validation result= ",result_validation
                    if realQ<minQ:
                        minQ=realQ
                        bestSubsetByQ = compresses_sample
                    if result_validation<minValidation:
                        minValidation=result_validation
                        bestSubsetByValidation = compresses_sample
                    #margin,compressed size,estimated_error, Q(d,e)-error bound
                count+=1
        with open("C:\Python_output\\"+str(data_description)+"CompressedBestRealQ.pickle",'wb') as fileName:
            print "writing file  to disk"
            pickle.dump(bestSubsetByQ,fileName)
        with open("C:\Python_output\\"+str(data_description)+"CompressedBestValidation.pickle",'wb') as fileName:
            print "writing file  to disk"
            pickle.dump(bestSubsetByValidation,fileName)
        return bestSubsetByQ,bestSubsetByValidation
    bestSubsetByQ,bestSubsetByValidation = find_best_subsets_by_real_Q_and_by_validation(train_data,sorted_edges,validation_data)
    print "executing on test data:"
    resultQ = 1-execute(bestSubsetByQ["compressed_sample"],test_data,distance_measure,gram_matrix=gram)
    resultV = 1-execute(bestSubsetByValidation["compressed_sample"],test_data,distance_measure,gram_matrix=gram)
    print "best Q: error on test = ",resultQ
    print "best validation: error on test = ",resultV
    return



