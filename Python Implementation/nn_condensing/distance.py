"""Distance metric for syscall sequences
"""

import numpy as np

# One way to compare system call traces is the distance between
# the call sequence and the best subsequence (BSS) in the
# window. The subsequence is obtained by removing some elements
# from the window. Although examples where BSS does not fairly
# reflect the similarity can be easily built (e.g. swapping of
# independent subsequences --- the distance between "1abxy2" and
# "1xyab2" should be 0 if xy is independent of ab), this measure
# is useful as the base level for comparison.

_INFINITY = float('infinity')

def d_bss(d_elem):
    """Returns function computing BSS distance.
    Arguments:
    d_elem - distance function between sequence elements.
    """

    def _d_bss(seq, win):
        """Computes BSS distance between sequence and window.
        Arguments:
        seq -- sequence (list of syscalls),
        win -- window (list of syscalls),
        Returns the distance.
        """
        # Dynamic programming:
        # d is distance matrix by indices of last consumed
        # element in seq and win.
        n, m = len(seq), len(win)
        d = np.zeros((n+1,m+1), dtype=float)

        # Set to infinity all combinations of indexes where the
        # sequence must either start before the window or end
        # after the window.
        for i in range(n+1):
            for j in range(0,i)+range(max(0, m-n+i)+1,m+1):
                d[i, j] = _INFINITY

        # Then compute distances recursively as the minimum
        # between matching the current symbol in the sequence
        # either now or earlier.
        for i in range(n):
            for j in range(i,max(0, m-n+i)+1):
                cost = d_elem(seq[i], win[j])
                d[i+1, j+1] = min(d[i, j] + cost, d[i+1, j])

        # Return the distance when both sequences are consumed.
        return d[n, m]

    return _d_bss
