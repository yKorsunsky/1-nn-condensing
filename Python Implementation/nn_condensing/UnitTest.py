__author__ = 'Genia'
from nn import read_data_set, fill_data, epsilon_net, consistent_pruning, execute
from RunningExample import distance_measure_L2_Skin, skin_line_parser
path = "E:\\pinhas code\\research\\"
relative_path = "NetTest.txt"
skinDataSet, tagged_Data = read_data_set("E:\\pinhas code\\research\\NetTest.txt", distance_measure_L2_Skin, skin_line_parser)
known, unknown = fill_data(tagged_Data, 9, 9)
print tagged_Data
reduced_known = epsilon_net(known, skinDataSet.get_distance_measure())
print reduced_known
net_after_pruning = consistent_pruning(reduced_known, skinDataSet.get_distance_measure())
print net_after_pruning
correct_percentage = execute(net_after_pruning, unknown, skinDataSet.get_distance_measure())
print "Reduced with pruning Data : Correct value is %f" % correct_percentage