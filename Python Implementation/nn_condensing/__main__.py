import sys
import argparse
import numpy as np
from numpy import array, zeros, argmin, inf,int

def main(args=None):
    """The main routine."""
    if args is None:
        args = sys.argv[1:]
    import RunningExample
    args_dict = {}
    print args
    if "--pruning" in args:
        av_p_s=RunningExample.run_experiment(return_average_pruning_size=True,compress_heirarchy=True,test_memory=False,train_data_upper_limit=100,repeat=10,test_1_nn_full_data=False,\
                                             test_1_nn_greedy=False,test_1_nn_greedy_pruning=False,generate_CSV=False,plot_compression=False)
        print "result=",av_p_s
    else:
        if "--plot" in args:
            RunningExample.plot_data(args[1])
        else:
            for dual in args:
                if dual.split('=')[0] in ['data_path','metric','train_data_size_inc_func','CSV_path']:
                    args_dict[dual.split('=')[0]] = dual.split('=')[1]
                else:
                    if dual.split('=')[0] in ['starting_train_data_size','test_data_size','train_data_upper_limit','train_data_size_inc_arg','repeat','plot_compression']:
                        args_dict[dual.split('=')[0]] = int(dual.split('=')[1])
                    else:
                        args_dict[dual.split('=')[0]] = (dual.split('=')[1] == 'True')
            print args_dict
            RunningExample.run_experiment(**args_dict)

if __name__ == "__main__":
    import editdistanceDTW
    print editdistanceDTW.eval("sss","a")
    #main("--pruning")


#nn_condensing data_path=E:\nn_condensing_output\Skin_NonSkin.txt starting_train_data_size=125 test_data_size=200 train_data_upper_limit=1000 metric=L1 train_data_size_inc_func=geometric train_data_size_inc_arg=2 CSV_path=E:\nn_condensing_output\CSV_All_Data.CSV
#nn_condensing -plot E:\nn_condensing_output CSV_All_Data.csv