"""Unit tests for distance metrics"""

import unittest
from .distance import *

class TestDistance(unittest.TestCase):
    """Test case for distance"""

    def test_bss(self):
        """Test best-subsequence distance"""
        def d_elem(a,b): return abs(ord(a)-ord(b))
        dist = d_bss(d_elem)
        self.assertEqual(dist("ab", "ab"), 0, "same are equal")
        self.assertEqual(dist("ab", "xaybz"), 0, "interspersed")
        self.assertEqual(dist("ab", "acd"), 1, "first better")
        self.assertEqual(dist("ab", "adc"), 1, "last better")


def suite():
    """returns testsuite with tests for distances"""
    return unittest.TestSuite([TestDistance(test) 
                               for test in [ "test_bss",
                                           ]
                               ])
