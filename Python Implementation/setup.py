import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "nn_condensing",
    version = "0.1",
    author = "Yevgeni Korsunsky",
    description = ("Sample compression for 1-nn."),
    packages = find_packages(),
    #dependencies
    install_requires = ["numpy>=1.8.2", "psutil>=3.3.0", "tabulate", "matplotlib"],
    entry_points = {
        'console_scripts': [
            'nn_condensing = nn_condensing.__main__:main'
        ]
    }
)