\documentclass{article}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{mysymbols}


\title{Collaboration between PaypaL/CyActive and Ben Gurion
	University on research of sample compression for nearest
	neighbor classifiers}
\author{David Tolpin, dtolpin@paypal.com}

\newcommand{\citep}[1]{\cite{#1}}
\newcommand{\citet}[1]{\cite{#1}}
\newcommand{\citeyearpar}[1]{\cite{#1}}

\newcommand{\X}{\calX}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\err}{\operatorname{err}}
\newcommand{\serr}{\widehat{\err}}
\newcommand{\ddim}{\operatorname{ddim}}

\begin{document}


\maketitle

\section{Our Problem}

We attempt to detect operating system processes performing
malicious activity by looking at their system call sequences
(traces). We are able to predict many patterns of malicious
activity and create system call traces of the patterns
offline. We also have vast amounts of traces of normal
processes.

We hope to use these data to devise a classifier that
predicts whether, and which kind of, malicious activity
is performed by each process. 

\begin{itemize}
\item Our problem is not to classify the data as belonging
to one or more classes of exploits, but rather to identify
whether it is an exploit (of a certain kind) \textbf{or} normal data.

\item We must guarantee a very low false positive rate, that
is reporting normal data as malicious must be an extremely
rare event. At the same time, we want to achieve as high
recall as possible.

\item We have a rather big training data set: 1 million
elements for malicious data, 10 million elements for normal
data is a conservative guess on the size of the training
data set. 

\item The training set is sparse  and the density varies
significantly.

\item We can spend a relatively long time pre-processing the
training data and training the classifier, but need to do
the classification very fast. One million samples per
second. 
\end{itemize}

Currently, we use a sliding-window scheme with
off-the-shelf machine learning algorithms to classify
traces. We experience performance and classification
efficiency problems, which we would like to solve using
alternative approaches.

\section{Sample compression for nearest neighbor}

The generality of the metric setting outlined above suggests a proximity-based approach.
The simple nearest neighbor (NN) classifier, introduced by
Fix and Hodges in 1951, continues to be a popular learning algorithm
among practitioners.
Despite the numerous sophisticated techniques developed in recent years,
this simple method continues to
``yield competitive results''
\citep{DBLP:journals/jmlr/WeinbergerS09}
and inspire papers in
``defense of nearest-neighbor based\ldots classification''
\citep{DBLP:conf/cvpr/BoimanSI08}.

An inherent shortcoming of NN-based methods is that naively, they require
storing and searching the entire training sample for classifying a test point,
which is prohibitive for large-scale datasets.
This shortcoming in the NN classifier led Hart \citet{DBLP:journals/tit/Hart68}
to pose the problem of sample compression.
Indeed, significant compression of the sample 
has the potential to simultaneously
address the issues of memory usage and NN search time,
while providing simple and sharp generalization bounds.
Hart had considered the minimum Consistent Subset
problem --- elsewhere called the Nearest Neighbor Condensing problem ---  
which seeks to identify for sample $S$
a minimal subset $S^* \subset S$ that is {\em consistent} with $S$, 
in the sense that 
the nearest neighbor in $S^*$ of every $x \in S$ possesses the same label as $x$.
This problem is known to be NP-hard \citep{Wil-91,Zuk-10}, and Hart 
provided a heuristic 
with runtime $O(n^3)$.
The runtime was recently improved
to $O(n^2)$
in \citet{DBLP:conf/icml/Angiulli05},
but neither paper gave performance guarantees.
The Nearest Neighbor Condensing problem has been the subject
of extensive research since its introduction \citep{gates72,ritter75,wilson00}. 
Yet surprisingly,
there were no known approximation algorithms for it --- all previous results on this 
problem were heuristics, lacking any non-trivial approximation guarantees. 
Conversely,
no strong hardness-of-approximation results for this problem were known.

\paragraph{Preliminary results.}
In our recent paper~\cite{gknips14}, we managed to
close this long-standing gap in solutions to
the Nearest Neighbor Condensing problem.
We gave a simple near-optimal algorithmic solution for this problem, 
while assuming only a metric space.
Define the {\em scaled margin} $\gamma < 1$ 
of a sample $S$ as the ratio of the minimum distance between opposite labeled 
points in $S$ to the diameter of $S$.
Our algorithm in~\cite[Theorem 1]{gknips14}
produces a consistent set 
$S' \subset S$ of size $\lceil 1/\gamma \rceil^{\ddim(S)+1}$
in time
\beqn
\label{eq:condense-runtime}
\min \{n^2, 2^{O(\ddim(S))} n \log(1/ \gamma) \}.
\eeqn
%where $\ddim(S)$
%is the doubling dimension of the space $S$.
Besides the obvious memory savings,
this result can significantly speed up evaluation on test points, and also
yields sharper and simpler generalization bounds than were previously known
(Theorem 3, ibid.).

To establish optimality, we complemented the algorithmic result
with an almost matching 
hardness-of-approximation lower-bound. 
Using a reduction from the Label Cover problem,
we showed 
that the Nearest Neighbor Condensing problem 
is NP-hard to approximate within
a
factor of $2^{(\ddim(S) \log (1/\gamma))^{1-o(1)}}$,
and this holds even for Euclidean spaces.
Note that the matching upper-bound is an absolute size guarantee, 
and stronger than an approximation estimate.

Additionally, we presented a simple heuristic 
to be applied in conjunction with the algorithm of compression algorithm
that achieves further sample compression.
The empirical performances of both our algorithm and heuristic 
seem encouraging, and should be implemented as part of a toolkit
for practitioners.


\section{Research topics}

\begin{enumerate}
\item efficiently discovering and adapting to distinct regions in an inhomogeneous space
\item developing sample compression techniques for regression
\item extending the metric space algorithms and analysis to non-metric spaces
\item bridging the nontrivial gap between theory and implementation.
\end{enumerate}


\subsection{Distance metrics}

Consider images, for example.
Although these can be naively represented as coordinate-vectors in
$\R^d$, the Euclidean (more generally, $\ell_p$) distance between the representative
vectors does not correspond well to the one perceived
by human vision. Instead, the earthmover distance
is commonly used in vision applications
\cite{DBLP:journals/ijcv/RubnerTG00}.
Yet representing earthmover distances using any fixed $\ell_p$ norm
unavoidably
introduces very large inter-point distortion \cite{NS07},
potentially 
%resulting in a useless representation of low fidelity.
corrupting the data geometry before the learning process has even begun.
Nor is this issue mitigated by kernelization, as kernels
necessarily embed the data in a Hilbert space, again implying the above distortion 
--- and so an approach of this type is ad-hoc, 
precluding a principled treatment of non-Euclidean data.
A similar issue arises
for strings: these can be naively treated as
vectors endowed with different $\ell_p$ metrics,
but a much more natural metric over strings is the edit distance, which is
similarly known to be strongly non-Euclidean \cite{AK10}.

These concerns have led researchers to seek out algorithmic and statistical
approaches that apply in
greater generality,
and a particularly fruitful direction has focused on metric spaces.
%to much more general spaces 
%with fewer structural constraints,
%%of simpler inherent structure,
%such as general metric spaces.
A {metric} is a notion of distance
satisfying
non-negativity, symmetry, and the triangle equality.
In this proposal, rather than assuming that the data is
represented as coordinate-vectors in a Hilbert
space, we treat the data points as atoms in a metric space, whose
structure is captured only by their pairwise distances. Now metric spaces may
be highly complex --- for example, they include infinite-dimensional Hilbert spaces --- 
and so particular attention must be paid to metric spaces with bounded {\em intrinsic dimension}, such as small covering-number growth rate 
or doubling dimension.
This paradigm captures some natural distance metrics, such as earthmover
and edit distances \cite{DBLP:journals/tit/GottliebKK14},
and has led to contributions in metric
classification \cite{LL06,BKL06,DBLP:journals/jmlr/LuxburgB04,DBLP:journals/tit/GottliebKK14,kw2014,kon-weiss-2014},
regression 
\cite{DBLP:conf/simbad/GottliebKK13,NIPS2009_1009,KpotufeDasgupta2012,DBLP:conf/nips/Kpotufe11,KpotufeDasgupta2012,DBLP:conf/nips/KpotufeG13},
clustering \cite{Dasgupta2005555,Dasgupta:2008:RPT:1374376.1374452,Ackermann:2010:CMN:1824777.1824779},
sample compression \cite{gknips14},
dimension reduction \cite{GK-13,DBLP:conf/alt/GottliebKK13},
differential privacy \cite{DBLP:journals/jmlr/ChaudhuriH11},
and boosting \cite{DBLP:conf/isaim/GutfreundKLR14}
(see also \cite{Dwork:2012:FTA:2090236.2090255, verma2012learning}).


While metric spaces are significantly more general than Hilbertian ones, 
they still do not capture many common distance functions used by practitioners.
These non-metric distances include
the Jensen-Shannon divergence, which appears in statistical applications
\citep{fuglede04,DBLP:conf/nips/GoodfellowPMXWOCB14},
$k$-median Hausdorff distances and $\ell_p$ distances with $0<p<1$,
which appear in vision applications \citep{jain94,DBLP:journals/pami/JacobsWG00}
--- all of which are
%These distances are readily recognized as 
semimetrics. 
An additional line of work by
\citet{jain94}
and
\citet{DBLP:journals/pami/JacobsWG00,DBLP:conf/iccv/JacobsWG98,DBLP:conf/nips/WeinshallJG98}
%established 
%championed
underscored
the 
%importance and 
effectiveness of non-metric distances in various applications (mainly vision),
and among these, semimetrics again play a prominent role
\citep{514678,546971,817410,232073,Jain:1997:RRH:273392.273401,790412}.
%We are not aware of any rigorous results for learning in semimetric spaces.


\subsection{Compression efficiency}

The generalizing power of sample compression was independently 
discovered by \citet{warmuth86,MR1383093}, and later elaborated upon by 
\citet{DBLP:journals/ml/GraepelHS05}.

For any distribution $\P$, any $n\in\N$ and any $0<\delta<1$,
with probability at least $1-\delta$ over the random sample 
$S\in(\X\times\set{-1,1})^n$, the following holds:
\begin{enumerate}
\item[(i)] If $\tilde S\subset S$ is consistent with $S$, then
\quad
${\ds
\err(\nu_{\tilde S}) \le \oo{n-|\tilde S|}\paren{
|\tilde S|\log n
+\log n+\log\oo\delta}.
}$
\item[(ii)] If $\tilde S\subset S$ is $\eps$-consistent with $S$, then
\quad
${\ds
\err(\nu_{\tilde S}) \le
\frac{\eps n}{n-|\tilde S|}+\sqrt{
\frac{
|\tilde S|\log n
+2\log n+\log\oo\delta}{2(n-|\tilde S|)}
}.
}$
\end{enumerate}


\subsection{Classification performance}

\textit{Not clear what you mean: past experiments? Future experiments?}

\section{Anticipated achievements}

\subsection{Malware Detection}

Ultimately, we expect to develop a new, improved classifier 
based on NN to be developed and deployed instead or
simultaneously with existing classifiers. We hope to achieve
better recall while still maintaining high precision, and
the ability to classify high volumes of data in real time.

\subsection{Predictive Engine}

As and intermediate step, we suggest to use sample
compression as a technique for improving generation
diversity in our predictive engine based on evolutationary
algorithms. Performance is still critical here, however the
classification speed is less critical and the sample set
size is much smaller.

\subsection{Timeline}

TODO

\bibliographystyle{plain}
\bibliography{mybib}

\end{document}
