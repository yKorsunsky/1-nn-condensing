package research;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.StringTokenizer;

public class skinSegmentationDATA extends DATA{
	
	public skinSegmentationDATA(BufferedImage im, StringTokenizer tok,String TAG){
		
		double mat[][] = new double[][]{{Integer.parseInt(tok.nextToken())},
				{Integer.parseInt(tok.nextToken())},{Integer.parseInt(tok.nextToken())}};
		super.setTAG(TAG);
		super.setData(mat);
	}
	
	private static BufferedImage GrayScale(BufferedImage im) {
		BufferedImage image = new BufferedImage(im.getWidth(), im.getHeight(),  
				BufferedImage.TYPE_BYTE_GRAY);
		for(int i=0;i<im.getWidth();i++){
			for(int j=0;j<im.getHeight();j++){
				Color c = new Color(im.getRGB(i, j));
				int pix = (int)(0.21*c.getRed() + 0.71*c.getGreen() +0.07*c.getBlue());
				image.setRGB(i, j,colorToRGB(pix,pix,pix,pix));
			}
		}

		return image;
	}
	
	public static int colorToRGB(int alpha, int red, int green, int blue) {
		int newPixel = 0;
		newPixel += alpha;
		newPixel = newPixel << 8;
		newPixel += red; newPixel = newPixel << 8;
		newPixel += green; newPixel = newPixel << 8;
		newPixel += blue;

		return newPixel;
	}
}
