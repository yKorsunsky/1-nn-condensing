package research;

public abstract class DATA {
	int id;
	String TAG;
	double [][]data_mat;
	
	public void setID(int id){
		this.id = id;
	}
	public void setTAG(String TAG){
		this.TAG = TAG;
	}
	
	public void setData(double [][]mat){
		data_mat = mat;
	}
}
