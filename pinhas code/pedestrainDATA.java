package research;

public class pedestrainDATA extends DATA{

	public pedestrainDATA(PGM im, String TAG) {
		double mat[][] = new double[im.pixels.length][im.pixels[0].length];
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				mat[i][j] = im.pixels[i][j];
			}
		}
		super.setData(mat);
		super.setTAG(TAG);
	}

}
