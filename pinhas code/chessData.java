package research;


public class chessData extends DATA {
	int [] WhiteKing;
	int [] WhiteRock;
	int [] BlackKing;

	public chessData(int WKC,int WKR,int WRC,int WRR,int BKC,int BKR,String name){
		WhiteKing = new int[]{WKC,WKR};
		WhiteRock = new int[]{WRC,WRR};
		BlackKing = new int[]{BKC,BKR};
		super.setTAG(name);
		super.setData(new double[][]{{WKC,WKR},{WRC,WRR},{BKC,BKR}});
	}
	public chessData(String WKC,String WKR,String WRC,String WRR,String BKC,String BKR,String name){
		super.setTAG(name);
		WhiteKing = new int[]{Integer.parseInt(WKC),Integer.parseInt(WKR)};
		WhiteRock = new int[]{Integer.parseInt(WRC),Integer.parseInt(WRR)};
		BlackKing = new int[]{Integer.parseInt(BKC),Integer.parseInt(BKR)};
		super.setData(new double[][]{{Integer.parseInt(WKC),Integer.parseInt(WKR)}
		,{Integer.parseInt(WRC),Integer.parseInt(WRR)}
		,{Integer.parseInt(BKC),Integer.parseInt(BKR)}});
	}
	
	public chessData(int WKC,int WKR,int WRC,int WRR,int BKC,int BKR){
		WhiteKing = new int[]{WKC,WKR};
		WhiteRock = new int[]{WRC,WRR};
		BlackKing = new int[]{BKC,BKR};
		super.setData(new double[][]{{WKC,WKR},{WRC,WRR},{BKC,BKR}});
	}
	


}