package research;

import java.util.StringTokenizer;

public class covtypeDATA extends DATA {
	private static double values[] = {};//{3686,360,66,1397,601,7117,254,254,254,7173};
	// notice to the max values if you change groups.. read covtype readme 
	public covtypeDATA(StringTokenizer tok, String TAG) {
		super.setTAG(TAG);
		double mat[][] = new double [1][tok.countTokens()-1];
		for(int i = 0; i < mat[0].length; i++){
			if(i < values.length)
				mat[0][i] = Double.parseDouble(tok.nextToken())/values[i];
			else
				mat[0][i] = Double.parseDouble(tok.nextToken());
		}
		super.setData(mat);
	}

	public String toString(){
		String data = "";
		for (int i = 0; i < super.data_mat[0].length; i++) {
			data += super.data_mat[0][i]+" ";
		}
		return data;
	}
}
