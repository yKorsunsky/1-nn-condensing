package research;

import java.util.ArrayList;

public class Net {
	private ArrayList<DATA> set; // original set
	private ArrayList<DATA> subSet; // set after net
	private double MinDistance;   
	private COMPAREABLE_DATA compare; // the way you compare between the data L1 , L2 Etc.


	public Net (ArrayList<DATA>  set,COMPAREABLE_DATA compare){
		this.set = set;
		this.compare = compare;
		subSet = new ArrayList<DATA>();

		findMinDist(set);// finding the minimum distance between + and -


		set.get(0).setID(0); // the id is for the use of  Consistent_pruning functaion.
		subSet.add(set.get(0));
		for (int i = 1; i < set.size(); i++) {
			set.get(i).setID(i);
			boolean insert = true;	
			// check if the current element is belong to the net or not
			for (int j = 0; j < subSet.size(); j++) {
				if(compare.dist(set.get(i),subSet.get(j)) < MinDistance){ 
					insert = false;
					j = subSet.size();
				}
			}
			if(insert)
				subSet.add(set.get(i));
		}		
	}
	/**
	 * Finds the smallest distance between + and -  
	 * where the tag of each element in the set is from the group {+ , - } 
	 * @param set - contains the data
	 * @param compare
	 */
	private void findMinDist(ArrayList<DATA> set) {
		MinDistance = Integer.MAX_VALUE;
		for(int i = 0; i < set.size(); i++){
			for(int j = i + 1; j < set.size(); j++){
				if(!set.get(i).TAG.equals(set.get(j).TAG)){
					double temp = compare.dist(set.get(i), set.get(j));
					if(temp < MinDistance)
						MinDistance = temp;
				}
			}
		}
	}


	public static ArrayList<DATA> Consistent_pruning(Net net){
		double C = 1.5;
		double radius = find_radius(net);
		ArrayList<DATA> v = net.getSubSet();
		for (double R = radius; R > 0; R/=C) {
			if(R - net.MinDistance < 0.0001 ) // stopping condition / ?? MinDistance where is it initiated??
				R = 0;
			for (int i = 0; i <  v.size(); i++){
				DATA p = v.get(i);
				boolean canRemove = true;
				for (int j = 0; j < v.size(); j++) {
					if(p.TAG != v.get(j).TAG
							&& net.dist(p, v.get(j)) < 2*R){
						canRemove = false;
						j = v.size();
					}
				}
				if(canRemove){			
					for (int k = 0; k < v.size(); k++) {
						if(p.id != v.get(k).id)		
							if(net.dist(p,v.get(k)) < R - net.MinDistance){
								v.remove(k);
								k--;
							}
					}
				}
			}

		}
		return v;
	}

	/**
	 * Find the greatest distance between an arbitrary point
	 * to the rest of the set.
	 * @param net
	 * @return
	 */
	private static double find_radius(Net net) {//?? why is it from point #0?
		double radius = 0;
		DATA arbitrary_point = net.getSetBefore().get(0);
		for(DATA d : net.getSetBefore()){
			double temp = net.compare.dist(arbitrary_point, d);
			if(temp > radius)
				radius = temp;
		}
		return radius;
	}
	public double getRadius(){
		return MinDistance;
	}
	public int getSizeBefore(){
		return set.size();
	}
	public int getSubSetSize(){
		return subSet.size();
	}
	public ArrayList<DATA> getSubSet(){
		return subSet;
	}
	public ArrayList<DATA> getSetBefore(){
		return set;
	}
	public double dist(DATA A, DATA B){
		return compare.dist(A, B);
	}
}
