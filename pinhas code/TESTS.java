package research;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.ArrayList;


public class TESTS {

	static String [] TAG;
	static String path = "E:\\pinhas code\\research\\";
	public static COMPAREABLE_DATA compare;
	public static int knownSize = 2000;
	public static int unknownSize = 2000;

	public static void main(String[] args) throws Exception {
		ArrayList<DATA> known,unknown;
		known = new ArrayList<DATA>(knownSize) ;    //remove?
		unknown = new ArrayList<DATA>(unknownSize);//remove?

		/*
		 * choose here which data set you want to use
		 * notice that each data set funcation Initializes 
		 * the "compare" (COMPAREABLE_DATA)  and the "TAG" (String array) differently
		 *  
		 */

		//	ChessSet(known,unknown);
		//	data_banknote_authentication_set(known,unknown);
		//  SkinSegmentationSet(known,unknown);
		//  connect4Set(known,unknown);
		//  covtype(known,unknown,1,2);
		//	TamilNadu_Electricity_Board(known,unknown,8,11); // bad data set dont use it.
		//	magicSet(known,unknown);
		//	shuttleSet(known,unknown);
		//	PedestrainSet(known,unknown,"2");
		
		  int NumberOfTests = 2;


		for (int i = 0; i < NumberOfTests; i++) {
			known = new ArrayList<DATA>(knownSize) ;    
			unknown = new ArrayList<DATA>(unknownSize);
			SkinSegmentationSet(known, unknown);
			System.out.println("Run : "+i);
			
			double result = round(execute(known,unknown,false),2);
			System.out.println("Before Size: "+knownSize+" ,Result: "+result+"%");
			Net net = new Net(known,compare);
			result = round(execute(net.getSubSet(),unknown,false),2);
			System.out.println("Net size: "+net.getSubSetSize()+" Radius: "+net.getRadius() +", Result: "+result+"%");

			ArrayList<DATA> v = Net.Consistent_pruning(net);
			result = round(execute(v,unknown,false),2);
			System.out.println("Size after consistent pruning: "+v.size() +", Result: "+result+"%");
		
		}
	
	}
	public static void findStandardDeviation(double[][] mat, double[] avg,
			double[] standard_deviation) {
		double apsSize = 1 / (double)mat[0].length;
		for (int i = 0; i < mat[0].length; i++) {
			for (int j = 0; j < standard_deviation.length; j++) {
				standard_deviation[j] += Math.pow(mat[j][i] - avg[j], 2);
			}
		}
		for (int j = 0; j < standard_deviation.length; j++) {
			standard_deviation[j] *= apsSize;
			standard_deviation[j] = Math.sqrt(standard_deviation[j]);
		}
	}
	public static void findAVG(double[][] mat, double[] avg) {
		for (int i = 0; i < mat[0].length; i++) {
			for (int j = 0; j < avg.length; j++) {
				avg[j] += mat[j][i];
			}
		}
		for (int j = 0; j < avg.length; j++) {
			avg[j] /= mat[0].length;
		}

	}
	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public static void saveToFile(String data,String path,boolean append){
		try{
			File file =new File(path+".txt");

			//if file doesnt exists, then create it
			if(!file.exists()){
				file.createNewFile();
			}

			//true = append file
			FileWriter fileWritter = new FileWriter(path+".txt",append);
			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
			bufferWritter.write(data);
			bufferWritter.newLine();
			bufferWritter.close();

		}catch(IOException e){
			e.printStackTrace();
		}

	}

	public static void PedestrainSet(ArrayList<DATA> known, ArrayList<DATA> unknown,String folder) throws IOException {
		TAG = new String[]{"ped_examples","non-ped_examples"};
		path = "C:\\Users\\pinhas\\Desktop\\����\\pedestrain\\"+folder+"\\";
		compare = new L1();
		int size = 4800;
		DATA [] All_H = new DATA[size];
		DATA [] All_NOT_H = new DATA[size+200];
		ArrayList<Integer> v1 = new ArrayList<Integer>();
		ArrayList<Integer> v2 = new ArrayList<Integer>();
		int i = 0;
		Random rand = new Random();
		for(int j=0;j<All_H.length;j++){
			i = rand.nextInt(size);
			while(v1.contains(i))
				i = rand.nextInt(size);
			v1.add(i);
			PGM im = new PGM(0,18,36);
			String name = ""+i;
			while(name.length() < 5)
				name = "0"+name;
			im.ReadPGM(path+TAG[0]+"\\"+"img_"+name+".pgm");
			All_H[j] = new pedestrainDATA(im,TAG[0]);
		}
		size = 5000;
		i = 0;
		for(int j=0;j<All_NOT_H.length;j++){
			i = rand.nextInt(size);
			while(v2.contains(i))
				i = rand.nextInt(size);
			v2.add(i);
			PGM im = new PGM(0,18,36);
			String name = ""+i;
			while(name.length() < 5)
				name = "0"+name;
			im.ReadPGM(path+TAG[1]+"\\"+"img_"+name+".pgm");
			All_NOT_H[j] = new pedestrainDATA(im,TAG[1]);
		}
		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}




	public static void shuttleSet(ArrayList<DATA> known, ArrayList<DATA> unknown
			) {
		compare = new L1();

		TAG = new String[]{"1","rest"};//{"1","2","3","4","5","6","7"};//
		//int sizes[] = {45586,50,171,8903,3267,10,13};
		int k1=0,k2=0;
		DATA [] All_H = new DATA[45586];//[8903];
		DATA [] All_NOT_H = new DATA[12414];//[3267];

		try{
			FileInputStream fstream = new FileInputStream(path+"shuttle.txt");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null)   {

				StringTokenizer tok = new StringTokenizer(strLine);
				if(strLine.substring(strLine.length()-1).equals("1")){
					All_H[k1++] = new shuttleDATA(tok,TAG[0]);
				}else {//if(strLine.substring(strLine.length()-1).equals("5")){
					All_NOT_H[k2++] = new shuttleDATA(tok,TAG[1]);
				}
			}

			//Close the input stream
			in.close();
		}catch (Exception e){//Catch exception if any
			e.printStackTrace();
		}

		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}





	/*
	 *  g = gamma (signal):     12332
   h = hadron (background): 6688
	 */
	public static void magicSet(ArrayList<DATA> known, ArrayList<DATA> unknown) {
		compare = new L1();
		TAG = new String[]{"g","h"};
		int k1=0,k2=0;
		DATA [] All_H = new DATA[12332];
		DATA [] All_NOT_H = new DATA[6688];
		try{
			FileInputStream fstream = new FileInputStream(path+"magic.txt");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null)   {
				StringTokenizer tok = new StringTokenizer(strLine,",");
				if(strLine.substring(strLine.length()-1).equals("g")){
					All_H[k1++] = new magicDATA(tok,TAG[0]);
				}else{
					All_NOT_H[k2++] = new magicDATA(tok,TAG[1]);
				}
			}

			//Close the input stream
			in.close();
		}catch (Exception e){//Catch exception if any
			e.printStackTrace();
		}
		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// bad data set.. dont use it!
	public static void TamilNadu_Electricity_Board(ArrayList<DATA> known, ArrayList<DATA> unknown,
			int id1, int id2) {
		compare = new L2();
		TAG = new String[]{  "Bank" , "AutomobileIndustry" , "BpoIndustry" , "CementIndustry" , 
				"Farmers1" , "Farmers2" , "HealthCareResources" , "TextileIndustry" , 
				"PoultryIndustry" , "Residential(individual)" , "Residential(Apartments)" ,
				"FoodIndustry" , "ChemicalIndustry" , "Handlooms" 
				, "FertilizerIndustry" ,"Hostel" ,"Hospital" , 
				"Supermarket", "Theatre" , "University" };
		int sizes [] = {1410,1403,1397,1403,1418,1405,1400,1405,2888,2867,2884,2905,2830
				,2887,2876,2857,2906,2874,2870,2896};
		int k1=0,k2=0;
		DATA [] All_H = new DATA[sizes[id1]];
		DATA [] All_NOT_H = new DATA[sizes[id2]];

		try{
			FileInputStream fstream = new FileInputStream(path+"Tamilnadu Electricity Board Hourly Readings.txt");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine = "";
			for (int i = 0; i < 12 && strLine!= null; i++) {
				strLine = br.readLine();
			}

			while ((strLine = br.readLine()) != null && strLine.length() > 1)   {
				StringTokenizer tok = new StringTokenizer(strLine,",");
				double d1 =  Double.parseDouble(tok.nextToken());
				double d2 =  Double.parseDouble(tok.nextToken());
				String tag = tok.nextToken();

				if(tag.equals(TAG[id1])){
					All_H[k1++] = new Electricity_BoardDATA(d1,d2,tag);
				}else if(tag.equals(TAG[id2])){
					All_NOT_H[k2++] = new Electricity_BoardDATA(d1,d2,tag);
				}
			}

			in.close();
		}catch (Exception e){
			e.printStackTrace();
		}
		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public static void covtype(ArrayList<DATA> known, ArrayList<DATA> unknown,
			int id1, int id2) {
		compare = new L1();
		TAG = new String[]{"1","2","3","4","5","6","7"};
		int sizes [] = {211840,283301,35754,2747,9493,17367,20510};
		int k1=0,k2=0;
		/*
		 *     1           2           3           4           5           6           7
		 *     211840      283301      35754       2747        9493        17367       20510
		 */

		id1--;
		id2--;
		DATA [] All_H = new DATA[sizes[id1]];
		DATA [] All_NOT_H = new DATA[sizes[id2]];
		try{
			FileInputStream fstream = new FileInputStream(path+"covertype\\covtype.data");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null)   {
				if((strLine.substring(strLine.length()-1)).equals(TAG[id1])){
					All_H[k1++] = new covtypeDATA(new StringTokenizer(strLine,","),TAG[id1]);
				}else if((strLine.substring(strLine.length()-1)).equals(TAG[id2])){
					All_NOT_H[k2++] = new covtypeDATA(new StringTokenizer(strLine,","),TAG[id2]);
				}
			}

			in.close();
		}catch (Exception e){
			e.printStackTrace();
		}
		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void SkinSegmentationSet(ArrayList<DATA> known, ArrayList<DATA> unknown) {
		compare = new L1();
		TAG = new String[]{"skin","non skin"};
		int k1=0,k2=0;
		DATA [] All_H = new DATA[50859];
		DATA [] All_NOT_H = new DATA[194198];
		try{
			FileInputStream fstream = new FileInputStream(path+"Skin_NonSkin.txt");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null)   {
				StringTokenizer tok = new StringTokenizer(strLine);
				BufferedImage im = null;//new BufferedImage(1,1,BufferedImage.TYPE_INT_RGB);

				if(strLine.substring(strLine.length()-1).equals("1")){
					All_H[k1++] = new skinSegmentationDATA(im,tok,TAG[0]);
				}else{
					All_NOT_H[k2++] = new skinSegmentationDATA(im,tok,TAG[1]);
				}
			}

			//Close the input stream
			in.close();
		}catch (Exception e){//Catch exception if any
			e.printStackTrace();
		}
		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




	public static void data_banknote_authentication_set(ArrayList<DATA> known, ArrayList<DATA> unknown) {
		TAG =new String[]{"0","1"};
		compare = new L1();
		int k1=0,k2=0;
		DATA [] All_H = new DATA[762];
		DATA [] All_NOT_H = new DATA[610];

		try{
			FileInputStream fstream = new FileInputStream(path+"data_banknote_authentication.txt");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null){
				StringTokenizer tok = new StringTokenizer(strLine,",");
				banknoteData d = new banknoteData(Double.parseDouble(tok.nextToken())
						,Double.parseDouble(tok.nextToken()),Double.parseDouble(tok.nextToken())
						,Double.parseDouble(tok.nextToken()),tok.nextToken());

				if(d.TAG.equals(TAG[1])){
					All_NOT_H[k2++] = d;
				}
				else{
					All_H[k1++] = d;

				}	

			}	
			in.close();
		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public static void ChessSet(ArrayList<DATA> known, ArrayList<DATA> unknown) {
		TAG = new String[]{"Under 11","more then 14"};
		String [] tag1 ={"fifteen","sixteen"};//,"thirteen","fourteen"};
		String [] notInUse = {"draw","eleven","twelve","thirteen","fourteen"};
		compare = new L1();

		int k1=0,k2=0;
		DATA [] All_H = new DATA[7506];//[13957];//
		DATA [] All_NOT_H = new DATA[2556];//[11303];//

		try{
			FileInputStream fstream = new FileInputStream(path+"chess\\krkopt.data");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null){
				StringTokenizer tok = new StringTokenizer(strLine,",");
				chessData d = new chessData((tok.nextToken().charAt(0)-'a')+""
						,tok.nextToken(),(tok.nextToken().charAt(0)-'a')+"",tok.nextToken()
						,(tok.nextToken().charAt(0)-'a')+"",tok.nextToken(),tok.nextToken());

				boolean yes = true;
				for (int j = 0; j < tag1.length; j++) {
					if(d.TAG.equals(tag1[j])){
						d.setTAG(TAG[1]);
						All_NOT_H[k2++] = d;
						j = tag1.length;
						yes = false;
					}
				}
				if(yes){
					for (int j = 0; j < notInUse.length; j++) {
						if(d.TAG.equals(notInUse[j])){
							yes = false;
							j = notInUse.length;
						}
					}
					if(yes){
						d.setTAG(TAG[0]);
						All_H[k1++] = d;
					}
				}

			}	
			in.close();
		}catch (Exception e){
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
		try {
			fillDATA(known,unknown,All_H,All_NOT_H);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	// fill randomly the known and unknown sets 

	private static void fillDATA(ArrayList<DATA> known, ArrayList<DATA> unknown,
			DATA[] All_H, DATA[] All_NOT_H) throws NumberFormatException, Exception {
		ArrayList<Integer> v1 = new ArrayList<Integer>();
		ArrayList<Integer> v2 = new ArrayList<Integer>();
		/*
		ArrayList<DATA> ALLH = new ArrayList<DATA>();
		ArrayList<DATA> ALLN = new ArrayList<DATA>();
		
		
		for (int i = 0; i < All_H.length; i++) {
			boolean add = true;
			for (int j = 0; j < ALLH.size(); j++) {
				if(compare.dist(All_H[i],ALLH.get(j)) == 0
						&& !All_H[i].TAG.equals(ALLH.get(j).TAG)){
					add = false;
					j = ALLH.size();
				}
			}
			if(add){
				ALLH.add(All_H[i]);
			}
		}
		System.out.println(ALLH.size());
		for (int i = 0; i < ALLH.size(); i++) {
			String data = ""+ALLH.get(i).toString();
			saveToFile(data,"covtype 3",true);
		}
		
		for (int i = 0; i < All_NOT_H.length; i++) {
			boolean add = true;
			for (int j = 0; j < ALLN.size(); j++) {
				if(compare.dist(All_NOT_H[i],ALLN.get(j)) == 0
						&& !All_NOT_H[i].TAG.equals(ALLN.get(j).TAG)){
					add = false;
					j = ALLN.size();
				}
			}
			if(add){
				ALLN.add(All_NOT_H[i]);
			}
		}
		System.out.println(ALLN.size());
		
		
		for (int i = 0; i < ALLN.size(); i++) {
			String data = ""+ALLN.get(i).toString();
			saveToFile(data,"covtype 4",true);
		}
		if(true)
		throw new Exception("super user stop");
	*/
		int i = 0;
		Random rand = new Random();
		// this adds items to Known array from both ALL_H and ALL_NOT_H, but makes sure there are no same items with different label
		if(known!=null)
			for(int j=0;j<knownSize;j++){
				if(v1.size() < All_H.length){
					i = rand.nextInt(All_H.length);
					while(v1.contains(i) && v1.size() < All_H.length) //?? why test v1.size again?
						//	|| Double.parseDouble(recognize(All_H[i],known,new boolean[known.size()])[1]) == 0)
						i = rand.nextInt(All_H.length);
					v1.add(i);
					boolean add = true;
					for (int j2 = 0; j2 < known.size(); j2++) {
						if(compare.dist(All_H[i],known.get(j2)) == 0
								&& !All_H[i].TAG.equals(known.get(j2).TAG)){//?? why not add only when different tag?
							add = false;
							j2 = known.size();
						}
					}
					if(add){//why not adding now?
						known.add(All_H[i]);
						j++;
					}
				}
				if(j < knownSize){
					if(v2.size() < All_NOT_H.length){
						i = rand.nextInt(All_NOT_H.length);
						while(v2.contains(i) && v2.size() < All_NOT_H.length)
							//	|| Double.parseDouble(recognize(All_NOT_H[i],known,new boolean[known.size()])[1]) == 0)
							i = rand.nextInt(All_NOT_H.length);
						v2.add(i);
						boolean add = true;
						for (int j2 = 0; j2 < known.size(); j2++) {
							if(compare.dist(All_NOT_H[i],known.get(j2)) == 0
									&& !All_NOT_H[i].TAG.equals(known.get(j2).TAG)){
								add = false;
								j2 = known.size();
							}
						}
						if(add)
						known.add(All_NOT_H[i]);
					}
				}
			}
		if(unknown!=null)
			for(int j=0;j<unknownSize;j++){
				if(v1.size() < All_H.length){
					i = rand.nextInt(All_H.length);
					while(v1.contains(i))
						i = rand.nextInt(All_H.length);
					v1.add(i);
					unknown.add(j++, All_H[i]);
				}
				if(j < unknownSize){
					if(v2.size() < All_NOT_H.length){
						i = rand.nextInt(All_NOT_H.length);
						while(v2.contains(i))
							i = rand.nextInt(All_NOT_H.length);
						v2.add(i);
						unknown.add(j,All_NOT_H[i]);
					}
				}
			}
		
	}

	/*
	 * use the known set to recognize all the unknown set
	 *   
	 */
	private static double execute(ArrayList<DATA> known, ArrayList<DATA> unknown
			,boolean print) throws Exception {
		long time = System.currentTimeMillis();
		double error = 0;
		for(int i=0;i<unknown.size();i++){
			if(unknown.get(i) != null){
				DATA a = unknown.get(i);
				String best_rec =recognize(a,known)[0];
				if(!best_rec.equals(a.TAG))
					error++;
			}
			else i = unknown.size();//why? does this mean the array is out of items? is unknown.size si the number of non-null items?
		}
		error/=unknown.size();
		double correct = 1 - error;
		correct*=100;
		if(print){
			System.out.println("The Success rate is: "+correct+"%");
			time = System.currentTimeMillis() - time;
			System.out.println("Running Time: "+time +" milisec");
			System.out.println("*****************************************************");
		}
		return correct;
	}

	/*
	 * use the known set to recognize unknown data element
	 */
	public static String [] recognize(DATA a,ArrayList<DATA> known) throws Exception {

		double best = Integer.MAX_VALUE;
		int index = -1;
		for(int i=0;i<known.size();i++){
			if(known.get(i) != null){
				DATA b = known.get(i);
				double sum = compare.dist(a, b);
				if(sum<best){
					best = sum;
					index = i;
				}
			}
			else 
				i = known.size();
		}
		if(index == -1) return new String[]{null,"-1"};
		return new String[]{known.get(index).TAG,best+""};
	}

	public static void connect4Set(ArrayList<DATA> known, ArrayList<DATA> unknown) {
		compare = new connect4NORM();
		compare = new L1();
		TAG = new String[]{"win","loss"};
		int k1=0,k2=0;
		DATA [] Win = new DATA[44473];
		DATA [] Loss = new DATA[16635];

		try{
			FileInputStream fstream = new FileInputStream(path+"connect-4.data");
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null){
				StringTokenizer tok = new StringTokenizer(strLine,",");
				double mat [][] = new double [6][7];
				for (int j = 0; j < mat.length; j++) {
					for (int k = 0; k < mat[0].length; k++) {
						String str = tok.nextToken();
						if(str.charAt(0) == 'x')
							mat[j][k] = 1;
						else if(str.charAt(0) == 'o')
							mat[j][k] = 2;
					}
				}
				String name = tok.nextToken();
				connect4Data data = new connect4Data(mat,name);
				if(data.TAG.equals(TAG[0])){
					Win[k1++] = data;
				}else if(data.TAG.equals(TAG[1])){
					Loss[k2++] = data;
				}
			}
			in.close();
		}catch (Exception e){
			e.printStackTrace();
		}
		try {
			fillDATA(known,unknown,Win,Loss);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
