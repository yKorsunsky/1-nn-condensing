package research;

public class Electricity_BoardDATA extends DATA{
	public Electricity_BoardDATA(double d1, double d2, String TAG){
		double [][] mat = new double[1][2];
		mat[0][0] = d1;
		mat[0][1] = d2;
		super.setData(mat);
		super.setTAG(TAG);
	}
}
