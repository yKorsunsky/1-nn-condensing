package research;

public class connect4NORM implements COMPAREABLE_DATA {

	@Override
	public double dist(DATA A, DATA B) {
		double dist = 0;
		for (int i = 0; i < A.data_mat.length; i++){
			for (int j = 0; j < A.data_mat[0].length; j++) {
				if((A.data_mat[i][j] == 1 && B.data_mat[i][j] == 2)
						||(A.data_mat[i][j] == 2 && B.data_mat[i][j] == 1)){
					dist += 2;
				}
				else if(((A.data_mat[i][j] == 1 || A.data_mat[i][j] == 2 )&& B.data_mat[i][j] == 0)
						||((B.data_mat[i][j] == 1 || B.data_mat[i][j] == 2 )&& A.data_mat[i][j] == 0)){
					dist++;
				}
			}
		}
		return dist;
	}

}