package research;

import java.util.StringTokenizer;

public class magicDATA extends DATA {

	public magicDATA(StringTokenizer tok, String TAG) {
		super.setTAG(TAG);
		double mat[][] = new double[1][9];
		for (int i = 0; i < mat[0].length; i++) {
			mat[0][i] = Double.parseDouble(tok.nextToken());	
		}
		super.setData(mat);
	}
	
}
