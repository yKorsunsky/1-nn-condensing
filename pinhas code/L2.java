package research;

public class L2 implements COMPAREABLE_DATA{

	@Override
	public double dist(DATA A, DATA B) {
		double sum = 0;
		for (int i = 0; i < A.data_mat.length; i++) {
			for (int j = 0; j < A.data_mat[0].length; j++) {
				sum += Math.pow(A.data_mat[i][j] - B.data_mat[i][j],2);
			}	
		}
		return Math.sqrt(sum);
	}

}
