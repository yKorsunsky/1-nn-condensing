package research;

public class chessNORM implements COMPAREABLE_DATA{


	@Override
	public double dist(DATA A, DATA B) {
	double sum = 0;
		
		double WR =0;
		double WK =
		Math.max(Math.abs(A.data_mat[0][0] - B.data_mat[0][0]),
				Math.abs(A.data_mat[0][1] - B.data_mat[0][1]));
		if((Math.abs(A.data_mat[1][0] - B.data_mat[1][0])==0
				&&Math.abs(A.data_mat[1][1] - B.data_mat[1][1])!=0)||
				(Math.abs(A.data_mat[1][0] - B.data_mat[1][0])!=0
				&&Math.abs(A.data_mat[1][1] - B.data_mat[1][1])==0))
			WR++;
		else if((Math.abs(A.data_mat[1][0] - B.data_mat[1][0])!=0
				&&Math.abs(A.data_mat[1][1] - B.data_mat[1][1])!=0))
			WR+=2;
		sum+= Math.max(Math.abs(A.data_mat[2][0] - B.data_mat[2][0]),
		Math.abs(A.data_mat[2][1] - B.data_mat[2][1]));
		sum = sum*sum + WK*WK + WR*WR;
		sum = Math.sqrt(sum);

		return sum;
	}
	
}